EESchema Schematic File Version 4
LIBS:extTemp-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	1550 1500 1550 1300
Wire Wire Line
	1550 1200 1550 1100
Wire Wire Line
	2800 1550 2900 1550
$Comp
L Transistor_BJT:MMBT3904 Q1
U 1 1 5D149538
P 1150 1300
F 0 "Q1" H 1350 1300 50  0000 L CNN
F 1 "MMBT3904" H 1000 1600 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 1350 1225 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N3904.pdf" H 1150 1300 50  0001 L CNN
	1    1150 1300
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:MMBT3904 Q2
U 1 1 5D14AE0F
P 1100 2000
F 0 "Q2" H 1291 2046 50  0000 L CNN
F 1 "MMBT3904" H 950 2250 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 1300 1925 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N3904.pdf" H 1100 2000 50  0001 L CNN
	1    1100 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 1200 1550 1200
Wire Wire Line
	1550 1300 1800 1300
$Comp
L Connector:Conn_01x02_Male J1
U 1 1 5D14C65E
P 2000 1300
F 0 "J1" H 2108 1481 50  0000 C CNN
F 1 "Conn_01x02_Male" H 1900 1400 50  0000 C CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-02A_1x02_P2.54mm_Vertical" H 2000 1300 50  0001 C CNN
F 3 "~" H 2000 1300 50  0001 C CNN
	1    2000 1300
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Male J2
U 1 1 5D14DB54
P 2000 2050
F 0 "J2" H 2108 2231 50  0000 C CNN
F 1 "Conn_01x02_Male" H 1900 2150 50  0000 C CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-02A_1x02_P2.54mm_Vertical" H 2000 2050 50  0001 C CNN
F 3 "~" H 2000 2050 50  0001 C CNN
	1    2000 2050
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x12 J3
U 1 1 5D15A3DB
P 3400 1750
F 0 "J3" V 3525 1696 50  0000 C CNN
F 1 "Conn_01x12" V 3616 1696 50  0000 C CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x12_P1.00mm_Vertical" H 3400 1750 50  0001 C CNN
F 3 "~" H 3400 1750 50  0001 C CNN
	1    3400 1750
	0    1    1    0   
$EndComp
Wire Wire Line
	2900 1550 3000 1550
Connection ~ 2900 1550
Wire Wire Line
	3000 1550 3100 1550
Connection ~ 3000 1550
Wire Wire Line
	3100 1550 3200 1550
Connection ~ 3100 1550
Wire Wire Line
	3200 1550 3300 1550
Connection ~ 3200 1550
Wire Wire Line
	3300 1550 3400 1550
Connection ~ 3300 1550
Wire Wire Line
	3400 1550 3500 1550
Connection ~ 3400 1550
Wire Wire Line
	3500 1550 3600 1550
Connection ~ 3500 1550
Wire Wire Line
	3600 1550 3700 1550
Connection ~ 3600 1550
Wire Wire Line
	3700 1550 3800 1550
Connection ~ 3700 1550
Wire Wire Line
	3800 1550 3900 1550
Connection ~ 3800 1550
Wire Wire Line
	1250 1500 1550 1500
Wire Wire Line
	1550 1100 1250 1100
Wire Wire Line
	1250 1100 950  1100
Wire Wire Line
	950  1100 950  1300
Connection ~ 1250 1100
Wire Wire Line
	1500 1800 1500 1950
Wire Wire Line
	1500 1950 1800 1950
Wire Wire Line
	1200 1800 1500 1800
Wire Wire Line
	1200 2200 1500 2200
Wire Wire Line
	1800 2050 1500 2050
Wire Wire Line
	1500 2050 1500 2200
Wire Wire Line
	1200 1800 900  1800
Wire Wire Line
	900  1800 900  2000
Connection ~ 1200 1800
Text Label 1650 1950 0    50   ~ 0
DXP
Text Label 1650 2050 0    50   ~ 0
DXN
Text Label 1600 1200 0    50   ~ 0
DXP2
Text Label 1600 1300 0    50   ~ 0
DXN2
$EndSCHEMATC

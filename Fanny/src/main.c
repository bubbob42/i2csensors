/*
  FannyCtl - PWM fan control for Amigas with I2C bus

  author: Henryk Richter <henryk.richter@gmx.net>


*/
#include <dos/dos.h>
#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/utility.h>
#define __CONSTLIBBASEDECL__
#include <proto/i2c.h>

#include "main.h"
#include "compiler.h"
#include <proto/i2csensors.h>
#include <proto/i2c.h>
#include <libraries/i2csensors.h>
#include "MAX31760.h"


/*----------------- safeguard against undesired compilation/linking ----------*/
/* problem: if linking without startup code, gcc2.95 will put constants from 
            functions before the functions themselves in the object files. To ensure
	    that the program will work as intended, a simple two-step start
	    is applied.
*/
SAVEDS int progstart(void);

#if 1
int startup(void)
{
 return progstart();
}
#else
/* variant b) put a structure that actually does a JMP 
   -> doesn't work with -fbaserel in GCC
*/
struct _safeguard_ {
	 UWORD magic;
	 ULONG ptr;
};
extern const struct _safeguard_ safeguard;
const struct _safeguard_ safeguard  = { 0x4ef9, (ULONG)progstart }; /* jmp progstart */
#endif
/*----------------- safeguard against undesired compilation/linking ----------*/

#define VERSION  0
#define REVISION 6
#define DATE     29.06.2019

/*----------------- options template -----------------------------------------*/
#define OPT_TEMPLATE "CHIP=DEV/K,VERBOSE=V/S,FIXEDPWM/K/N,SAVEEEPROM=SAVE/S,NOFAULT/K/N,TMIN/K/N,TMAX/K/N,PMIN/K/N,PMAX/K/N,RAMPSPEED/K/N,TSOURCE/K/N"

struct progopts {
	BYTE *devname;
	ULONG verbose;   /* verbose outputs */
	ULONG *fixedpwm; /* disable LUT if given */
	ULONG eeprom;    /* save current settings to EEPROM */
	ULONG *nofault;  /* disable Fan Fault (1) or enable (0), if enabled (0), then full speed if a fan fails or is not connected */
	ULONG *mintemp;  /* lower temperature point for adaptive fan control (below: minpwm or 10%)  */
	ULONG *maxtemp;  /* upper temperature point for adaptive fan control (above: maxpwm or 100%) */
	ULONG *minpwm;   /* minimum PWM duty cycle (def: 10%) */
	ULONG *maxpwm;   /* maximum PWM duty cycle (def:100%) */
	ULONG *ramprate;   /* speed change rate (0-3) */
	ULONG *tempsource; /* temperature reading source (0-2) */
};
/*----------------- options template -----------------------------------------*/


/*----------------- local protos and variables -------------------------------*/
extern void MemSet( VOID *, UBYTE, ULONG );
extern int  my_scan_i2c( long);
LONG hexstring2ulong( char *str );
LONG i2cwriteread( LONG address, BYTE *wdata, LONG wsize, BYTE *rdata, LONG rsize );
void sane_defaults( UBYTE *regs0 );
void print_status( UBYTE *regs0, UBYTE *regs1, UBYTE *lut, LONG verbose );

/* see at end of file */
const char dosname[];
const char utilityname[];
const char printfmt[];
const char i2cname[];
const char opt_template[];
const char unknownstr[];

SYSBASETYPE        *SysBase;
struct DosLibrary  *DOSBase;
UTILITYBASETYPE    *UtilityBase;
struct Library *I2CSensorsBase;
struct Library *I2C_Base;

#define xstr(a) str(a)
#define str(a) #a

const char verstring[] = "$VER: FannyCtl " xstr(VERSION) "." xstr(REVISION) " (" xstr(DATE) ")";


/*----------------- main function --------------------------------------------*/
SAVEDS int progstart( void )
{
 ULONG adr = BASEADDR_DEF;
 UBYTE regs0[24]; /* CR1....USER+7 */
 UBYTE regs1[11]; /* PWMR...SR */
 UBYTE LUT[49]; /* 48 LUT bytes plus header byte */

 UBYTE hdr[4];
 int ret;
 struct RDArgs *rdargs;
 struct progopts opts;

 SysBase = *(SYSBASETYPE**)0x4L;
 DOSBase = (struct DosLibrary*)OpenLibrary( (char*)dosname,37);

 if( !DOSBase )
 	return 20;
 
 I2CSensorsBase = OpenLibrary( (STRPTR)SENSORSNAME, 1 );
 if( !I2CSensorsBase )
 	I2C_Base = OpenLibrary( "i2c.library", 1 );

 if( (!I2C_Base) && (!I2CSensorsBase) )
 {
	Printf("Fatal Error! Can neither open i2csensors.library, nor i2c.library. Exiting\n");
	CloseLibrary( (struct Library*)DOSBase );
	return 20;
 }

 UtilityBase = (UTILITYBASETYPE*)OpenLibrary( (char*)utilityname,37);


 /* parameters */
 MemSet( &opts, 0, sizeof( struct progopts ));

 if( (rdargs = ReadArgs( (char*)opt_template,(LONG*)&opts,NULL) ) )
 {
 }
 else
  Printf("Syntax: %s\n",(ULONG)opt_template);

 if( opts.devname )
 	adr = hexstring2ulong( opts.devname );
 if( opts.verbose )
 	Printf("Using device address %lx\n",adr );


 /* read current status from chip (control registers, user registers */
 hdr[0] = MAX31760_CR1; 
 ret = i2cwriteread( adr, hdr, 1, regs0, 24 );
 if( !(ret & 0xf) )
 {
	Printf("Cannot read control registers from chip (code %ld)\n",ret);
	ret = 10;
	goto Quit;
 }
 ret = 0;

 /* Apply sane defaults */
 sane_defaults( regs0 );
 
 /* directly write defaults to CR regs */
 hdr[0] = MAX31760_CR1;
 hdr[1] = regs0[MAX31760_CR1];
 hdr[2] = regs0[MAX31760_CR2];
 i2cwriteread( adr, hdr, 3, NULL, 0 );


 do
 {
   /*
     0 = default: 3.125% per second
     1 = 6.25%
     2 = 12.5%
     3 = immediate
   */
   if( opts.ramprate )
   {
    	if( *opts.ramprate > 3 )
    	{
		Printf("Parameter Error: ramp rate index is 0...3\n");
		ret = 1;
		break;
    	}
	regs0[MAX31760_CR3] &= 0xCF;
	regs0[MAX31760_CR3] |= (*opts.ramprate&3)<<4;
	hdr[0] = MAX31760_CR3; 
	hdr[1] = regs0[MAX31760_CR3];
	i2cwriteread( adr, hdr, 2, NULL, 0 );
   }

   /*
     0 = remote (default)
     1 = local
     2 = max(local,remote)
   */ 
   if( opts.tempsource )
   {
    	if( *opts.tempsource > 2 )
    	{
		Printf("Parameter Error: temperature reference index is 0...2 (remote,local,max. of both)\n");
		ret = 1;
		break;
    	}

	if( *opts.tempsource == 2 )
	{
		regs0[MAX31760_CR1] |= 2;
	}
	else
	{
		regs0[MAX31760_CR1] &= 0xFC;
		regs0[MAX31760_CR1] |= (*opts.tempsource)^1;
	}

	hdr[0] = MAX31760_CR1; 
	hdr[1] = regs0[MAX31760_CR1];
	i2cwriteread( adr, hdr, 2, NULL, 0 );
   }
 
   /* 
     0=default (fault detect and TACH monitoring for both fans)
     1=ignore FAN1
     2=ignore FAN2
     3=ignore both (useless)
     4=don`t go into fail detection mode (fixedpwm only)
   */
   if( opts.nofault )
   {
   	ret  = 1;
   	if( *opts.nofault )
   	 ret = 0;

   	if(opts.verbose)
   		Printf("Fan1 ignore %lx, Fan2 ignore %lx, Fault Detection and Emergency mode %lx\n",
   		        *(opts.nofault) & 1,
   		       (*(opts.nofault) & 2)>>1, ret);

	regs0[MAX31760_CR3] = ( regs0[MAX31760_CR3] & 0xB4) | (3^(*(opts.nofault)&3)) | (8^(ret<<3));
	hdr[0] = MAX31760_CR3; 
	hdr[1] = regs0[MAX31760_CR3];
	i2cwriteread( adr, hdr, 2, NULL, 0 );

	/*
	   problem: if fans are ignored, we cannot read their RPM by I2C
	   solution in fixed-speed mode: just write the emergency register with the fixed PWM
	*/
	hdr[0] = MAX31760_FFDC;
	if( (!ret) && (opts.fixedpwm) )
	{
		ret = UDivMod32(*opts.fixedpwm*255,100); /* (*opts.fixedpwm*167116)>>16; */
		regs0[MAX31760_FFDC]=ret;
	}
	else
		ret = 0xff;
	hdr[1] = ret;
	i2cwriteread( adr, hdr, 2, NULL, 0 );
	ret = 0;
   }

   /* arg: 1..100 */  
   if( opts.fixedpwm )
   {
   
	if( (*opts.fixedpwm < 0) || (*opts.fixedpwm > 100) )
	{
		Printf("PWM percentage must be 1...100 (argument: %ld)\n",*opts.fixedpwm);
		ret = 1;
		break;
	}
	
	/* percent 0...100 -> 0...0xff */
	ret = UDivMod32(*opts.fixedpwm*255,100); /* (*opts.fixedpwm*167116)>>16; */
	
   	if(opts.verbose)
		Printf("Setting fixed PWM %ld%% (HW: %ld)\n",*opts.fixedpwm,ret);

	regs0[MAX31760_CR2] |= 1;	/* set DFC bit for fixed-speed mode */
		
	hdr[0] = MAX31760_CR2;
	hdr[1] = regs0[MAX31760_CR2];	
	i2cwriteread( adr, hdr, 2, NULL, 0 );

	hdr[0] = MAX31760_PWMR;
	hdr[1] = ret;  /* PWM duty cycle for fixed speed */
	i2cwriteread( adr, hdr, 2, NULL, 0 );

	ret = 0;
	break;
   }

   /* no fixed PWM: see what the user wants with temperature controls */
   if( (opts.mintemp) || (opts.maxtemp) )
   {
	ULONG minpwm,maxpwm,mint,maxt,ct,rng;
	ULONG L;

	/* normalize PWM to 255 */
	minpwm = (opts.minpwm) ? *opts.minpwm : 10;
	maxpwm = (opts.maxpwm) ? *opts.maxpwm : 100;
	minpwm = UDivMod32(minpwm*255,100); 
	maxpwm = UDivMod32(maxpwm*255,100); 

	mint = 18;
	if( opts.mintemp )
		mint = *opts.mintemp;
	else
		Printf("min temp not given, using default %ld\n",(ULONG)mint);

	maxt = 60;
	if( opts.maxtemp )
		maxt = *opts.maxtemp;
	else
		Printf("max temp not given, using default %ld\n",(ULONG)maxt);

	if( minpwm > 255 )
		minpwm = 255;
	if( maxpwm > 255 )
		maxpwm = 255;

	if( maxpwm < minpwm )
	{
		Printf("Parameter error: minimum PWM > maximum PWM\n");
		ret = 1;
		break;
	}
	if( mint < 18 )
	{
		Printf("Warning: min temperature for LUT is 18�C\n");
		mint = 18;
	}
	if( maxt > 110 )
	{
		Printf("Warning max. temperature for LUT is 110�C\n");
		maxt = 110;
	}
	if( mint > maxt )
	{
		Printf("Parameter error: minimum temp > maximum temp\n");
		ret = 1;
		break;
	}

	/* LUT lower part below tmin */
	L  = 1;
	ct = 16;
	while( ct < mint )
	{
		LUT[L++] = minpwm;
		ct+=2;
		if( L > 48 )
			break;
	}

	/* middle part with linear interpolation */
	rng = UDivMod32( ((maxpwm-minpwm)<<8), (maxt-mint+1) );
	minpwm <<= 8;
	while( ct < maxt )
	{
		minpwm  += rng;
		minpwm  += rng;
		LUT[L++] = (minpwm>>8);
		ct+=2;
	}

	/* upper part beyond tmax */
	while( ct <= 110 )
	{
		LUT[L++] = maxpwm;
		ct += 2;
	}

	/* Apply parameters: clear DFC bit in CR2 */
	regs0[MAX31760_CR2] &= 0xFE;
	hdr[0] = MAX31760_CR2;
	hdr[1] = regs0[MAX31760_CR2];
	i2cwriteread( adr, hdr, 2, NULL, 0 );

	/* Apply parameters: write LUT */
	LUT[0] = MAX31760_LUT;
	i2cwriteread( adr, LUT, 49, NULL, 0 );
   }

 }
 while(0);

 /* proceed only when there was not any error */
 if( !ret ) 
 {
 	/* get current status */
 	hdr[0] = MAX31760_PWMR; 
 	ret = i2cwriteread( adr, hdr, 1, regs1, 11 );
 	if( ret & 0xf) 
	{
		hdr[0] = MAX31760_LUT;
		i2cwriteread( adr, hdr, 1, LUT+1,48);
	
		print_status( regs0, regs1, LUT+1, opts.verbose );
		ret = 0;
	}
	else
	{
		Printf("Cannot read status registers from chip (code %ld)\n",ret);
		ret = 10;
	}
 }

 /* write parameters from RAM to EEPROM */
 if( opts.eeprom )
 {
 	if( ret )
		Printf("Return code is not 0, skipping EEPROM write operation!\n");
 	else
 	{
 		UBYTE ct=10;
 		
 		hdr[0] = MAX31760_EEX;
 		hdr[1] = 0x1F; /* write whole RAM to EEPROM */
 		i2cwriteread( adr, hdr, 2, NULL, 0 );
		Printf("Writing RAM to EEPROM, please wait 0.5s\n");
		Delay(26);

 		while(ct--)
		{
 			ret = i2cwriteread( adr, hdr, 1, hdr, 1 );
 			if( (hdr[0] == 0) && (ret&0xf) )
 			{
 				Printf("Success\n");
 				ct = 127;
 				break;
 			}
 			else
 				Delay(5);
 		}
 		if( ct != 127 )
 			Printf("EEPROM write failure\n");
	}
 }

 /* ret = 0; */

Quit:

 if( rdargs ) FreeArgs(rdargs);

 CloseLibrary( (struct Library*)DOSBase);
 if( UtilityBase )
 	CloseLibrary( (struct Library*)UtilityBase );
  if( I2CSensorsBase )
  	CloseLibrary( (struct Library*)I2CSensorsBase );
  if( I2C_Base )
  	CloseLibrary( (struct Library*)I2C_Base );

 return ret;
}



int  my_scan_i2c( long verbose)
{
 LONG total = 0;
 USHORT dummy=0;
 LONG id,rd,wr;
 struct Library *I2C_Base;

// rtc_list = i2c_RTCChips();
 I2C_Base = OpenLibrary( "i2c.library",39 );

 if( !I2C_Base )
 {
 	Printf("Cannot open i2c.library V39\n");
	return total;
 }

 for( id = 0; id < 256 ; id+=2 )
 {
  rd = ( ReceiveI2C( id+1, 1, (BYTE*)&dummy) & 0xff) ? 1 : 0;
  wr = ( SendI2C(    id,   0, (BYTE*)&dummy) & 0xff) ? 1 : 0;

  if( (rd&wr) )
  {
	/* now check array for ID */
	Printf("Chip Address 0x%lx/0x%lx\n",id,id+1);
  }
 }

 if( !total )
 {
	Printf("No supported RTC chips found\n");
 }

 CloseLibrary( I2C_Base );

 return total;
}

/*
  ensure useful defaults
*/
void sane_defaults( UBYTE *regs0 )
{

 /* def: apply 25 kHz PWM (assume typical PWM fans) */
 regs0[MAX31760_CR1] &= 0x23;	/* keep temp selection and hysteresis */
 regs0[MAX31760_CR1] |= 0x18;	/* 11b<<4 for 25 kHz PWM frequency, def: 00b<<4 for 33 Hz */

 /* assume PWM fans with square pulses for TACH signal */
 regs0[MAX31760_CR2] &= 0x31;   /* def: no standby, keep spin-up setting, keep FF setting, keep direct fan control */

}

/*
  Input:
   regs0: CR1...USER+7
   regs1: PWMR...SR
   lut:   PWM lookup table
*/
void print_status( UBYTE *regs0, UBYTE *regs1, UBYTE *lut, LONG verbose )
{
 ULONG tmp,tmp2;

 /* interpret CR1 */
 tmp = regs0[MAX31760_CR1];

 if( tmp & 2 )
  Printf("Temperature source is maximum of local and remote reading.\n");
 else
  Printf("Temperature source: %s\n",(ULONG)( (tmp&1) ? "remote transistor." : "MAX31760 chip temperature." ) );
 
 if( verbose )
 {
 	switch( tmp & 24 )
 	{
		case 0:
			tmp2=33;break;
		case 8:
			tmp2=150;break;
		case 16:
			tmp2=1500;break;
		default: /* case 24: */
			tmp2=25000;break;
	 }
	 Printf("PWM Frequency %ld Hz, Polarity %s\n", tmp2, (ULONG)( (tmp&4)?"negative":"positive") ); 
	 Printf("Lookup Table Hysteresis %ld �C\n",(ULONG)( (tmp&32) ? 4 : 2) );
 }

 /* interpret CR2 */
 tmp = regs0[MAX31760_CR2];
 Printf("Operating mode %s\n",(ULONG)( (tmp&1) ? "fixed PWM" : "temperature controlled" ) );

 if( verbose )
  Printf("Forced fan Spin-Up %ld\n",(tmp&32)>>5);

 /* interpret CR3 */
 if( verbose )
 {
	 tmp = regs0[MAX31760_CR3];
	 Printf("PWM Duty-Cycle Ramp Rate ");
	 switch( tmp & 48 )
	 {
		case 0:
			Printf("3.125%% per second.\n");break;
		case 16:
			Printf("6.25%% per second.\n");break;
		case 32:
			Printf("12.5%% per second.\n");break;
		default: /* case 48: */
			Printf("immediate (fastest).\n");break;
	 }
 }

 tmp = regs0[MAX31760_CR3];
 switch( tmp & 3 )
 {
	case 0:	Printf("Both Fan TACH signals ignored/unavailable.\n");break;
	case 1:	Printf("Fan1 TACH monitoring is on, Fan2 is ignored.\n");break;
	case 2: Printf("Fan1 is ignored, Fan2 TACH monitoring is on.\n");break;
	case 3: Printf("Fan1 and Fan2 TACH monitoring is active.\n");break;
 }

 if( verbose )
  Printf("Fan Fault Duty Cycle %ld\n",(ULONG)regs0[MAX31760_FFDC] );

 if( (verbose) || (regs0[MAX31760_CR1]&1) )
  Printf("Fixed PWM duty cycle %ld\n",(ULONG)regs1[MAX31760_PWMR-MAX31760_PWMR] );

 /* dump current rotation speed and temperatures */
 Printf("Current PWM duty cycle %ld\n",(ULONG)regs1[MAX31760_PWMV-MAX31760_PWMR] );
 if( regs0[MAX31760_CR3]&3 ) /* only if fans are not ignored */
 {
 	if( regs0[MAX31760_CR3]&1 )
	{
		tmp = (((ULONG)regs1[MAX31760_TC1H-MAX31760_PWMR])<<8)+(ULONG)regs1[MAX31760_TC1L-MAX31760_PWMR];
		Printf("Fan1 TACH speed %ld RPM, ",UDivMod32(3000000,tmp) );
	}
 	if( regs0[MAX31760_CR3]&2 )
	{
	 	tmp = (((ULONG)regs1[MAX31760_TC2H-MAX31760_PWMR])<<8)+(ULONG)regs1[MAX31760_TC2L-MAX31760_PWMR];
		Printf("Fan2 TACH speed %ld RPM ",UDivMod32(3000000,tmp));
	}
	Printf("\n");
 }

 /* temps */
 tmp   = (ULONG)regs1[MAX31760_LTL-MAX31760_PWMR]; /* 2^-1 2^-2 2^-3 0 0 0 0 0 */
 tmp2  = (tmp&128) ? 500 : 0; /* UGH! do better. */
 tmp2 += (tmp&64)  ? 250 : 0;
 tmp2 += (tmp&32)  ? 125 : 0;
 Printf("Local chip Temp %ld.%03ld �C, ",(ULONG)regs1[MAX31760_LTH-MAX31760_PWMR],tmp2);
 tmp   = (ULONG)regs1[MAX31760_RTL-MAX31760_PWMR]; /* 2^-1 2^-2 2^-3 0 0 0 0 0 */
 tmp2  = (tmp&128) ? 500 : 0;
 tmp2 += (tmp&64)  ? 250 : 0;
 tmp2 += (tmp&32)  ? 125 : 0;
 Printf("Remote transistor Temp %ld.%03ld �C\n",(ULONG)regs1[MAX31760_RTH-MAX31760_PWMR],tmp2);

 if( (verbose) && (lut) )
 {
 	ULONG rng;
 	
	Printf("LUT:\n18| ");
	for( rng = 0 ; rng < 48 ; rng ++ )
	{
		if( (rng) && !(rng&7) )
			Printf("\n%2ld| ",rng*2+18);
		Printf("%3ld ",lut[rng] );
	}
	Printf("\n");
 }

}


void MemSet( VOID *ptr, UBYTE c, ULONG size )
{
 UBYTE *p = (UBYTE*)ptr;

	while( size-- )
		*p++ = c;
}

/*
  get a single unsigned number from hex string 
  Input:  string
  Output: 
*/
LONG hexstring2ulong( char *str )
{
 int valid = 1;
 LONG ret  = 0;

 if( !str )
 	return 0;
 if( str[0] == '$' )
  str++;
 else
  if( (str[0] == '0' ) && (str[1] == 'x' ) )
   str += 2;

 while(valid)
 {
	if( (str[0] >= '0' ) && (str[0] <= '9') )
		ret = (ret<<4) + str[0] - '0';
	else
	{
		if( (str[0] >= 'A' ) && (str[0] <= 'F') )
			ret = (ret<<4) + str[0] + 10 - 'A';
		else
		{
			if( (str[0] >= 'a' ) && (str[0] <= 'f') )
				ret = (ret<<4) + str[0] + 10 - 'a';
			else
				valid = 0;
		}
	}
	str++;
 }

 return ret;
}

/* 
  I2C transaction
  write some data (NULL for no write) and read some data afterwards (NULL for no read)

  return: 0         -> fail
          res & 0xf -> ok 
*/
LONG i2cwriteread( LONG address, BYTE *wdata, LONG wsize, BYTE *rdata, LONG rsize )
{
 LONG ret = 0;

 if( I2CSensorsBase )
 {
 	i2c_Obtain( address );

	if ( (wdata) && (wsize) )
		ret = i2c_Send( address, wsize, wdata );
	if ( (rdata) && (rsize) )
		ret = i2c_Receive( address, rsize, rdata );

	i2c_Release( address );

	return ret;
 }
 if( I2C_Base )
 {
	if ( (wdata) && (wsize) )
		ret = SendI2C( address, wsize, wdata );
	if ( (rdata) && (rsize) )
		ret = ReceiveI2C( address, rsize, rdata );
	return ret;
 }
 return ret;
}



/* some strings down here */
const char opt_template[] = OPT_TEMPLATE;
const char dosname[] = "dos.library";
const char printfmt[] = "%ld -> %lx\n";
const char i2cname[] = "i2c.library";
const char unknownstr[] = "unknown";
const char utilityname[] = "utility.library";


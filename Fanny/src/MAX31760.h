/*
  MAX31760.h
  
  author: Henryk Richter <henryk.richter@gmx.net>
  
  definitions for MAX31760 fan controller chip
  
*/
#ifndef _MAX31760_H
#define _MAX31760_H

/* registers */
#define MAX31760_CR1	0x00
#define MAX31760_CR2	0x01
#define MAX31760_CR3	0x02
#define MAX31760_FFDC	0x03
#define MAX31760_MASK	0x04
#define MAX31760_IFR	0x05
#define MAX31760_RLSH	0x06
#define MAX31760_RHSH	0x07
#define MAX31760_LOTSH	0x08
#define MAX31760_LOTSL	0x09
#define MAX31760_ROTSH	0x0a
#define MAX31760_ROTSL	0x0b
#define MAX31760_LHSH	0x0c
#define MAX31760_LHSL	0x0d
#define MAX31760_TCTH	0x0e
#define MAX31760_TCTL	0x0f

/* USER (8 Bytes) */
#define MAX31760_USER	0x10
/* 48 byte LUT */
#define MAX31760_LUT	0x20

#define MAX31760_PWMR	0x50
#define MAX31760_PWMV	0x51
#define MAX31760_TC1H	0x52
#define MAX31760_TC1L	0x53
#define MAX31760_TC2H	0x54
#define MAX31760_TC2L	0x55
#define MAX31760_RTH	0x56
#define MAX31760_RTL	0x57
#define MAX31760_LTH	0x58
#define MAX31760_LTL	0x59
#define MAX31760_SR	0x5a
#define MAX31760_EEX	0x5b



	
#endif /* _MAX31760_H */
/*
  file:   config.h

  configuration file parsing, taglist output

  author: Henryk Richter <henryk.richter@gmx.net>

*/
#ifndef _INC_CONFIG_H
#define _INC_CONFIG_H

#ifndef _INC_I2CCLASS_H
#include <libraries/i2cclass.h> /* will load utility/tagitem.h */
#endif

#ifndef _INC_COMPILER_H
#include "compiler.h"
#endif


/*-----------------------------------------------------------------*/
/*--------------- swap FIX1616 by float when defined --------------*/
#define  USE_MATH
#include "mathwrap.h"


/*-----------------------------------------------------------------*/
/* some fiddly defines to keep the parser code relatively indendent
   of the i2c_sensorbase stuff, if so desired...
   
   This was added to keep library bases in a well known structure,
   whose contents are referenced for library calls

   If you want to get rid of it, see that the Lib bases of
   SysBase,Utilitybase and DOSBase are global and keep the
   defines below empty
*/
#define BASENAME  i2c_sensorbase
#define BASETYPE  struct i2c_sensorbase 
#define BASEARG   BASENAME,
#define BASEPROTO ASMR(a6) BASETYPE* ASMREG(a6),
#define BASEDEF   ASMR(a6) BASETYPE*BASENAME ASMREG(a6),
#define BASEPTR  BASETYPE *BASENAME


/*-----------------------------------------------------------------*/
#ifndef I2C_SENSORBASE_DECL
struct  i2c_sensorbase; /* make struct known */
#define I2C_SENSORBASE_DECL
#endif

struct confparse;


/*----------------- keyword to tag item mapping -------------------*/
struct keytag {
	ULONG	tag;	 /* tag value to apply                     */
	char	*key;	 /* key to search for                      */
	LONG    keychars;/* avoid strlen() for each cmp            */ 
	ULONG	argtype; /* argument type to set in tag data       */
};

#define KARG_INVALID   0
#define KARG_STRING    1 /* String                                 */
#define KARG_INT       2 /* Integer                                */
#define KARG_FIX1616   3 /* 16.16 fractional integer               */
#define KARG_KEYCUSTOM 4 /* String parsed for custom keyword       */
#define KARG_KEYTYPE   5 /* String that's parsed for type keyword  */
#define KARG_HEXSTRING 6 /* hex string                             */


/*-----------------  public functions -----------------------------*/

ASM struct TagItem*config_parse(BASEPROTO 
                                ASMR(a0) BYTE *          ASMREG(a0),
                                ASMR(a1) struct TagItem* ASMREG(a1)
                               );

ASM void config_free(           BASEPROTO
                                ASMR(a0) struct TagItem * ASMREG(a0)
                               );


/*-----------------  private functions ----------------------------*/

ASM void *AllocVecPooled(       BASEPROTO
                                ASMR(a0) void * ASMREG(a0), 
                                ASMR(d0) LONG ASMREG(d0) 
                               );
ASM void FreeVecPooled(         BASEPROTO
                                ASMR(a0) void * ASMREG(a0), 
                                ASMR(a1) void * ASMREG(a1)
                               );
ASM LONG conf_checksection(     BASEPROTO
                                ASMR(a0) struct confparse * ASMREG(a0)
                               );
ASM LONG conf_grow(             BASEPROTO
                                ASMR(a0) struct TagItem **  ASMREG(a0), 
                                ASMR(a1) LONG *             ASMREG(a1)
                               );
ASM LONG conf_add_global(       BASEPROTO
                                ASMR(a0) struct confparse * ASMREG(a0), 
			        ASMR(d0) ULONG              ASMREG(d0), 
			        ASMR(d1) ULONG              ASMREG(d1)
			       );
ASM LONG conf_add_device(       BASEPROTO
                                ASMR(a0) struct confparse * ASMREG(a0)
                               );
ASM void conf_mapkeyarg(        BASEPROTO
                                ASMR(a0) struct confparse * ASMREG(a0)
                               );
ASM void trim_confbuf(          BASEPROTO
                                ASMR(a0) struct confparse * ASMREG(a0)
                               );

ASM LONG conf_setsectionString( BASEPROTO
                                ASMR(a0) struct confparse * ASMREG(a0), 
                                ASMR(d0) LONG               ASMREG(d0), 
			        ASMR(a1) BYTE *             ASMREG(a1) 
			       );
ASM LONG conf_setsectionHEX(    BASEPROTO
                                ASMR(a0) struct confparse * ASMREG(a0), 
                                ASMR(d0) LONG               ASMREG(d0), 
			        ASMR(a1) BYTE *             ASMREG(a1) 
			       );
ASM LONG conf_setsectionInt(    BASEPROTO
                                ASMR(a0) struct confparse * ASMREG(a0), 
                                ASMR(d0) LONG               ASMREG(d0), 
			        ASMR(a1) BYTE *             ASMREG(a1) 
			       );
ASM LONG conf_setsectionKey(    BASEPROTO
                                ASMR(a0) struct confparse * ASMREG(a0),
                                ASMR(d0) LONG               ASMREG(d0), 
			        ASMR(a1) BYTE *             ASMREG(a1),
			        ASMR(a2) struct keytag *    ASMREG(a2)
			       );
BYTE *conf_delwhitespace( BYTE *str );

#if  defined(USE_MATH) || defined(USE_MATHLIB)
/* semi public :-) */
ASM void PrintFloat(            BASEPROTO
                                ASMR(d0) FRACNUM ASMREG(d0) 
                               );
#endif

void MemClear(                  void *, LONG
                               );


#endif /* _INC_CONFIG_H */

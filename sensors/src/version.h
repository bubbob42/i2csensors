/*
  version.h

  (C) 2018 Henryk Richter <henryk.richter@gmx.net>

  version and date handling
*/
#ifndef _INC_VERSION_H
#define _INC_VERSION_H

#define LIBVERSION  2
#define LIBREVISION 0
#ifndef LIBEXTRA
#define LIBEXTRA
#endif
/* #define DEVICEEXTRA Beta */
#define LIBDATE     23.4.21

#endif

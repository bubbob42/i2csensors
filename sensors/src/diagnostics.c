/*
  standalone (static) diagnostic test for I2C Sensors library
  
  (C) 2018 Henryk Richter

  This is just the test application to reduce turnaround
  times in library development. All relevant library code
  is compiled in here.

*/
#include <dos/dos.h>
#include <dos/exall.h>
#include <proto/dos.h>
#include <exec/memory.h>
#include <proto/exec.h>
#include <proto/utility.h>

#include "i2cclass_sensor.h"
#include "config.h"

#define nTypes 7
const LONG senlist[nTypes] = { I2C_VOLTAGE,I2C_CURRENT,I2C_TEMP,I2C_FAN,I2C_PRESSURE, I2C_POWER, I2C_HUMIDITY };
const char*sennames[nTypes]= { "Voltage",  "Current",  "Temperature","Fan","Pressure","Power",   "Humidity" ,}; 
LONG sennum[nTypes];

LONG find_problem(  struct i2c_sensorbase * );
LONG check_configs( struct i2c_sensorbase * );

extern void StringCopy( STRPTR d, STRPTR s ); /* steal from library */
extern void StringCat( STRPTR d, STRPTR s ); /* steal from library */


int main( int argc, char**argv )
{
	struct i2c_sensorbase *i2c_sensorbase;

	i2c_sensorbase = (struct i2c_sensorbase*)AllocMem(sizeof( struct i2c_sensorbase ), MEMF_PUBLIC );

	/* i2c lib base buffer is used for error checking */
	find_problem( i2c_sensorbase );

	FreeMem( i2c_sensorbase, sizeof( struct i2c_sensorbase ));

	return 0;
}

#if 1
#define xstr(a) str(a)
#define str(a) #a
static const unsigned short hdr[] = { 0x2456,0x4552,0x3a20 }; /* "$VER: " */
const char *_LibVersionString = xstr(LIBNAME) " " xstr(LIBVERSION) "." xstr(LIBREVISION) xstr(LIBEXTRA) " (" xstr(LIBDATE) ")\r\n";
#endif

/*
  lib didn`t open: find out what the problem was
*/
LONG find_problem( struct i2c_sensorbase *i2c_sensorbase )
{
	struct Library *tst;
	LONG bigprob;
	
	tst = OpenLibrary( (STRPTR)"i2c.library",39 );
	if( !tst )
	{
		Printf("cannot open i2c.library v39+\n");
	}
	else
	{
		Printf("i2c.library present\n");
		CloseLibrary(tst);
	}

	tst = OpenLibrary( (STRPTR)"i2csensors.library",1 );
	if( !tst )
	{
		Printf("cannot open i2csensors.library v1+\n");
	}
	else
	{
		Printf("i2csensors.library opened\n");
		CloseLibrary(tst);
	}

	/* check config files */
	bigprob = 0;
	if( i2c_sensorbase )
	{
		Printf("checking internal i2csensors.library base\n");
		LibInit( i2c_sensorbase, (0), (0) ); /* i2c_sensorbase = i2c_SensorOpen(); */
		if( !(i2c_sensorbase->utilitylib) )
		{
			Printf( (STRPTR)"utility.library not opened\n");
			bigprob = 1;
		}
		if( !(i2c_sensorbase->doslib))
		{
			Printf( (STRPTR)"dos.library not opened\n");
			bigprob = 1;
		}
		if( !(i2c_sensorbase->i2clib))
			Printf( (STRPTR)"i2c.library not opened\n");
		if( !(i2c_sensorbase->mathieeesingbas))
		{
			Printf( (STRPTR)"mathieeesingbas.library not opened\n");
			bigprob = 1;
		}
	}
	if( !bigprob )
	{
		Printf("checking i2csensors.library config files\n");
		check_configs( i2c_sensorbase );
	}
	else
		Printf("essential system libraries missing, skipping config file check\n");

	return 0;
}


LONG check_configs( struct i2c_sensorbase *i2c_sensorbase )
{
  BPTR lock;
  LONG more,ret;
  struct ExAllControl *eac;
  struct ExAllData ead,*cur;
  const STRPTR a = "~(#?.info)";
  const STRPTR pth = "Devs:Sensors/";
  BYTE path[108+32];
  BYTE pat[24];
  struct TagItem *conf,*conf2,*curitem,*conf3;
  
  ParsePatternNoCase( a, pat, 24 );

  lock = Lock( pth, ACCESS_READ );

  ret  = 0;
  if( lock )
  {
	eac = (struct ExAllControl*)AllocDosObject( DOS_EXALLCONTROL, (0) );
	if( eac )
	{
		eac->eac_LastKey = 0;
		eac->eac_MatchString = pat;
		do
		{
			more = ExAll( lock, &ead, sizeof(ead), ED_NAME, eac ); 
			if( eac->eac_Entries > 0 )
			{
				cur = &ead;
				do
				{
					/* main part: read each config */
					StringCopy( path, pth /* "Devs:Sensors/" */ );
					StringCat( path, cur->ed_Name );
					Printf("%s (%ld)\n",(ULONG)path,eac->eac_Entries);
					
					conf = config_parse(BASEARG (BYTE*)path,(0) );
					if( !conf )
						Printf("cannot parse %s\n",(ULONG)path);
					else
					{
						LONG havedev=0;
						conf2=conf;
						while( (curitem = NextTagItem(&conf2)) )
						{
							
							if( curitem->ti_Tag == I2C_DEVENTRY )
							{
								LONG ct=0;
								
								havedev++;
								Printf("Device Tag: ");
								conf3 = (struct TagItem*)curitem->ti_Data;
								while( (curitem = NextTagItem(&conf3)) )
								{
									ct++;
									/* Printf("%lx, ",curitem->ti_Tag); */
								}
								if( ct >= 3 )
									Printf("%ld attribute tags\n",ct);
								else
									Printf("%ld attribute tags (check file: there should be more!)\n",ct);
							}
							else
							{
							 if( curitem->ti_Tag == I2SEN_MEMHANDLE )
							 	Printf("Mem Handle Tag\n");
							 else
								Printf("conf tag %lx\n",curitem->ti_Tag);
							}
						}
						if( !havedev )
						{
							Printf("No device definition found in config, i.e. something like: [blah]\n");
						}
						config_free(BASEARG conf);
					}
				}
				while( (cur=cur->ed_Next) );
			}
		}
		while(more);	 
	 
	  	FreeDosObject( DOS_EXALLCONTROL, eac );
	}
	else /* if( eac ) */
	{
		Printf("Cannot obtain ExAllControl from DOS.library\n");
	}
	UnLock( lock );
#if 0
	/* translate config tags into internal struct 
	   less flexible but faster (and private, too)
	*/
	i2c_SensorTranslateConfig( i2c_sensorbase );

	/* initialize all sensors */
	i2c_SensorInit( i2c_sensorbase );
#endif
	ret = 1;
  }
  else /* if(lock) */
  {
  	Printf("cannot get a lock on %s\n",(ULONG)pth);
  }
  
  return ret;
}


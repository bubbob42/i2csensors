/*
  file:   custom_tk60t.c

  proposal for I2C class for sensor reading

  author:  Henryk Richter <henryk.richter@gmx.net>

  concept: This file contains the sensor reading and
           parsing functions for the LTC2990 on 
           the Matze A3000/4000 68040/68060 card. The
           temperature calculation requires a look at
           VCC to compensate the voltage reading over
           the temperature sensing resistor. Hence,
           custom code.
           
           Custom sensors are defined in three places:
           i2cclass_sensor.h
            definition of custom sensor Tag name
             I2SENC_*
           config.c
            definition of keyword to Tag name
             struct keytag confcustomtag[] = {}
           i2cclass_sensor.c
            function call, see "case I2SEN_CUSTOM:"
  
*/
#include <exec/memory.h>
#include <proto/exec.h>
#include <proto/utility.h>
#include <proto/dos.h>
#include <proto/timer.h>
#include <dos/exall.h>

#define __CONSTLIBBASEDECL__ /* some GCC targeted includes carry this attribute */ 
#include <proto/i2c.h>

#include "macros.h"
#include "config.h"
/* _I2CCLASS_INTERNAL triggers redirected library pointers 
   (DOSBase,I2CBase,UtilityBase etc.)
*/
#define  _I2CCLASS_INTERNAL
#include "i2cclass_sensor.h"
#include "debug.h"

#include "i2cclass_sensor_private.h"


#define CHKRES if( !(res & 0xf) ){ return 0;}

/*
   PRIVATE
   custom temperature reading code for Matze`s TK60

         R1        R2     Rx
   VCC--[1k]--*--[470R]--[Rx]--GND
              |
              V4
              
   Rx is 780 Ohm at 25�C (XC68060,MC68060)
   temperature coefficient 2.8 Ohm / �C
*/
#define R1 1000L
#define R2 470L

LONG SAVEDS custom_TK60TEMP(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s)
{
	LONG res;
	LONG VCC,V4,Vr,Rx8,Tx16;
	UBYTE *p = (UBYTE*)&i2c_sensorbase->readbuffer[ 0 ];
	//BYTE* sp = (BYTE*)p; /* signed input */

	/* first: read VCC (we could also use the V1 sensor
	   on this card) but the VCC measurement is independent 
	   of the voltage divider resistors */
	p[0] = 0x0E;
	SendI2C( s->i2caddr, 1, p );
	res = ReceiveI2C( s->i2caddr, 2, p ); /* VCC */
	CHKRES
	VCC = (( (ULONG)(0x3F & p[0]) )<<8) + (ULONG)p[1];

	p[0] = 0x0C;
	SendI2C( s->i2caddr, 1, p );
	res = ReceiveI2C( s->i2caddr, 2, p ); /* V4 */
	CHKRES
	V4 = (( (ULONG)(0x3F & p[0]) )<<8) + (ULONG)p[1];

	/* Raw VCC is in 305.18�V units, with an offset of 2.5V */
	/* V4 is also in 305.18�V units. Since we only need the ratio V4/VCC,
	   we don`t need to normalize. */
	VCC  += 0x1FFF; /* +2.5V = 8191*305.18�V    */

	D(("V4RAW %ld VCC %ld\n",V4,VCC));

	if( !VCC )	/* sanity: avoid /0 */
		return 0;

	V4  <<= 16;       /* get some fractional bits */
	V4   += (VCC>>1); /* rounding */
	Vr    = V4/VCC;   /* voltage ratio * 65536    */

	D(("Vr %ld\n",Vr));

	/* 
	   formula (unscaled, voltage divider solved for Rx)
	    Rx = (Vr`*(1000+470) - 470)/(1-Vr`)
	    
	   due to Vr scaling -> Rx normalized
	    Rx = (Vr*(1000+470) - (470<<16) ) / ( (1<<16) - Vr)

	   eight more bits in Rx
	    (Rx<<8) = ((Vr*(1000+470) - (470<<16) )<<4) / (((1<<16)-Vr)>>4)
	*/
	Rx8  = (( Vr*(R1+R2) - (R2<<16) )<<4) / (( (1<<16)-Vr )>>4);
	D(("Rx8 %ld Rx %ld\n",Rx8,(Rx8>>8) ));

	/* Rx8 should be around (780...1000)<<8 */
	Rx8<<= 11; /* (12 Bit )<<8<<11 = 31 Bit */

	/* this leaves 4 bit of fraction */
	Tx16   = ( Rx8-(780L<<19)+45875 ) / 91750L; /* /(2.8<<15) */
	Tx16 <<= 12; /* bolster to 16 bit fraction */
	Tx16  += (25<<16);

	D(("Tx16 %ld Tx %ld\n",Tx16,(Tx16>>16) ));
	
	res = Tx16;
	
	return res;
}


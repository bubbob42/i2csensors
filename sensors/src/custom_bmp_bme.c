/*
  file:   custom_bmp_bme.c

  proposal for I2C class for sensor reading

  author:  Henryk Richter <henryk.richter@gmx.net>

  concept: This file contains the sensor reading and
           parsing functions for Bosch sensors,
           beginning with the BMP280 and BME680.
           
           Custom sensors are defined in three places:
           i2cclass_sensor.h
            definition of custom sensor Tag name
             I2SENC_*
           config.c
            definition of keyword to Tag name
             struct keytag confcustomtag[] = {}
           i2cclass_sensor.c
            function call, see "case I2SEN_CUSTOM:"
  
*/
#include <exec/memory.h>
#include <proto/exec.h>
#include <proto/utility.h>
#include <proto/dos.h>
#include <proto/timer.h>
#include <dos/exall.h>

#define __CONSTLIBBASEDECL__ /* some GCC targeted includes carry this attribute */ 
#include <proto/i2c.h>

#include "macros.h"
#include "config.h"
/* _I2CCLASS_INTERNAL triggers redirected library pointers 
   (DOSBase,I2CBase,UtilityBase etc.)
*/
#define  _I2CCLASS_INTERNAL
#include "i2cclass_sensor.h"
#include "debug.h"

#include "i2cclass_sensor_private.h"


#define CHKRES if( !(res & 0xf) ){ return 0;}

/*
   PRIVATE
   custom temperature reading code for Bosch BME680
*/
static LONG t_fineBME680 = 25*256/5*100; /* def: 25�C room temperature (before first temp reading) */
LONG SAVEDS custom_BME680T(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s)
{
	LONG  res;
	LONG  par_t1,par_t2,par_t3,temp_adc;
	LONG  var1,var2,var3;
	UBYTE *p = (UBYTE*)&i2c_sensorbase->readbuffer[ 0 ];
	BYTE* sp = (BYTE*)p; /* signed input */

	/* get Temperature calibration */
	p[0] = 0x8A;
	SendI2C( s->i2caddr, 1, p );
	res = ReceiveI2C( s->i2caddr, 3, p ); /* 8a/8b=par_t2,8c=par_t3 */
	CHKRES
	par_t2 = (((LONG)sp[1])<<8) + (LONG)p[0]; /* doc says LSB/MSB */
	par_t3 = sp[2];
	
	D(("parT2 0x%lx 0x%lx parT3 0x%lx (%ld %ld)\n",(ULONG)p[0],(ULONG)p[1],(ULONG)p[2],par_t2,par_t3));

	p[0] = 0xE9;
	SendI2C( s->i2caddr, 1, p );
	res = ReceiveI2C( s->i2caddr, 2, p ); /* E9/EA=par_t1 */
	CHKRES
	par_t1 = (((LONG)p[1])<<8) + (ULONG)p[0];

	D(("parT1 0x%lx 0x%lx (%ld)\n",(ULONG)p[0],(ULONG)p[1],par_t1));

	/* get Temperature reading */	
	p[0] = 0x22;
	SendI2C( s->i2caddr, 1, p );
	res = ReceiveI2C( s->i2caddr, 3, p ); /* 0x22/0x23/0x24 = temp_adc */
	CHKRES
	temp_adc = (((LONG)p[0])<<12) + (((ULONG)p[1])<<4) + (((ULONG)p[2])>>4); /* 20 bit */

	D(("temp_adc 0x%lx 0x%lx 0x%lx (%ld)\n",(ULONG)p[0],(ULONG)p[1],(ULONG)p[2],temp_adc));

	var1 = (temp_adc>>3) - (par_t1<<1);
	var2 = (var1*par_t2)>>11;
	var3 = ( (((var1>>1)*(var1>>1))>>12)*(par_t3<<4) ) >> 14;
	t_fineBME680 = var2 + var3;

	D(("temp_comp %ld (�C*100)\n", (((t_fineBME680*5)+128)>>8) ));

	res = SDivMod32( ((var2+var3)<<7), 10 ); /* T = ((var2+var3)<<7)/10; */ 
	D(("T %ld (�C*65536)\n",res));	/* res = (temp in �C) * 65536 */

	return res;
}

/*
   PRIVATE
   custom pressure reading code for Bosch BME680
*/
LONG SAVEDS custom_BME680P(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s)
{
	LONG  res;
	LONG  par_p1,par_p2,par_p3,par_p4,par_p5,par_p6;
	LONG  par_p7,par_p8,par_p9,par_p10;
	LONG  var1,var2,var3,press_raw;
	ULONG press_comp;
	UBYTE *p = (UBYTE*)&i2c_sensorbase->readbuffer[ 0 ];
	BYTE* sp = (BYTE*)p; /* signed input */

	/* get Pressure calibration */
	p[0] = 0x8E;
	SendI2C( s->i2caddr, 1, p );
	res = ReceiveI2C( s->i2caddr, 19, p ); /* 0x8E...0xA0, inclusive */
	CHKRES
	par_p1 = ((LONG)p[ 0x8F - 0x8E ]<<8)  + (ULONG)p[ 0x8E - 0x8E ];
	par_p2 = ((LONG)sp[ 0x91 - 0x8E ]<<8) + (ULONG)p[ 0x90 - 0x8E ];
	par_p3 = (LONG)sp[ 0x92 - 0x8E ];
	par_p4 = ((LONG)sp[ 0x95 -0x8E ]<<8) + (ULONG)p[ 0x94 - 0x8E ];
	par_p5 = ((LONG)sp[ 0x97 -0x8E ]<<8) + (ULONG)p[ 0x96 - 0x8E ];
	par_p6 = (LONG)sp[ 0x99 - 0x8E ];
	par_p7 = (LONG)sp[ 0x98 - 0x8E ];
	par_p8 = ((LONG)sp[ 0x9D - 0x8E ]<<8) + (ULONG)p[ 0x9C - 0x8E ];
	par_p9 = ((LONG)sp[ 0x9F - 0x8E ]<<8) + (ULONG)p[ 0x9E - 0x8E ];
	par_p10= (LONG)p[0xA0-0x8E];

	D(("parP1 0x%lx parP2 0x%lx parP3 0x%lx parP4 0x%lx\n",par_p1,par_p2,par_p3,par_p4));

	/* get pressure measurement */
	p[0] = 0x1F;
	SendI2C( s->i2caddr, 1, p );
	res = ReceiveI2C( s->i2caddr, 3, p );
	CHKRES
	press_raw = (((LONG)p[0])<<12) + (((ULONG)p[1])<<4) + (((ULONG)p[2])>>4); /* 20 bit */


	var1 = (t_fineBME680>>1) - 64000;
	var2 = ( ( ((var1>>2)*(var1>>2))>>11 )*par_p6 )>>2;
	var2 = var2 + ((var1*par_p5)<<1);
	var2 = (var2>>2) + (par_p4<<16);
	var1 = (((((var1>>2)*(var1>>2))>>13) * (par_p3<<5))>>3) + ((par_p2*var1)>>1);
	var1 = var1>>18;
	var1 = ((32768+var1)*par_p1)>>15;

	press_comp = UMult32( (1048576-press_raw) - (var2>>12) , 3125 );

	if( press_comp < 0x40000000 )
	 press_comp = UDivMod32( (press_comp<<1), var1 );
	else
	 press_comp = UDivMod32( press_comp, var1 ) << 1;

	var1 = (SMult32( par_p9, ( SMult32( press_comp>>3, press_comp>>3 ) >> 13 ) ) + 2048 ) >> 12;
	var2 = (SMult32( (press_comp>>2), par_p8 ) + 4096 ) >> 13; 
	var3 = (SMult32( SMult32( (press_comp>>8), par_p10 ), SMult32( (press_comp>>8), (press_comp>>8) ) ))>>17;
	
	press_comp = press_comp + ( (var1 + var2 + var3 + (par_p7<<7))>>4);

	D(( (STRPTR)"Pressure %ld Pa\n",press_comp ));

	/* (press_comp is in Pa) * 65536/100 = hPa in Fix 16.16 */

	/* separate by high part and low part and do some integer voodoo */
	var1 =  press_comp & 0x0000ffff;
	var2 = (press_comp & 0xffff0000)<<12; /* see below for additional <<4 */

	var3 = UDivMod32( UMult32( var1, 65536 )+50, 100 );
	var2 = UDivMod32( var2+50, 100 )<<4;

	press_comp = var2 + var3;

	return (LONG)press_comp;
}

LONG SAVEDS custom_BME680H(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s)
{
	LONG  res;
	LONG  par_h1,par_h2,par_h3,par_h4,par_h5,par_h6,par_h7;
	LONG  var1,var2,var3,var4,var5,var6,hum_adc,temp_scaled;
	UBYTE *p = (UBYTE*)&i2c_sensorbase->readbuffer[ 0 ];
	BYTE* sp = (BYTE*)p; /* signed input */

	/* get Humidity calibration */
	p[0] = 0xE1;
	SendI2C( s->i2caddr, 1, p );
	res = ReceiveI2C( s->i2caddr, 8, p ); /* 0xE1...0xE8, inclusive */
	CHKRES
	par_h1 = ((ULONG)p[ 0xE3 - 0xE1 ]<<4) + ((ULONG)p[ 0xE2 - 0xE1 ] &0xf);
	par_h2 = ((ULONG)p[ 0xE1 - 0xE1 ]<<4) + ((ULONG)p[ 0xE2 - 0xE1 ] >>4 );
	par_h3 = (LONG)sp[ 0xE4 - 0xE1 ];
	par_h4 = (LONG)sp[ 0xE5 - 0xE1 ];
	par_h5 = (LONG)sp[ 0xE6 - 0xE1 ];
	par_h6 = (ULONG)p[ 0xE7 - 0xE1 ];
	par_h7 = (LONG)sp[ 0xE8 - 0xE1 ];

	D(("parH1 0x%lx parH2 0x%lx parH3 0x%lx parH4 0x%lx\n",par_h1,par_h2,par_h3,par_h4));
	D(("parH5 0x%lx parH6 0x%lx parH7 0x%lx\n",par_h5,par_h6,par_h7));

	p[0] = 0x25;
	SendI2C( s->i2caddr, 1, p );
	res = ReceiveI2C( s->i2caddr, 2, p ); /* 0x25...0x26, inclusive */
	CHKRES

	hum_adc = (((ULONG)p[0])<<8) | (ULONG)p[1];

	D(("Hum_ADC %ld\n",hum_adc));

	temp_scaled = (((LONG)t_fineBME680 * 5) + 128) >> 8;

	var1 = (hum_adc - ((LONG) ((LONG) par_h1 * 16)))
		- (((temp_scaled * (LONG) par_h3) / ((LONG) 100)) >> 1);
	var2 = (par_h2* (((temp_scaled * (LONG) par_h4) / ((LONG) 100))
		+ (((temp_scaled * ((temp_scaled * (LONG) par_h5) / ((LONG) 100))) >> 6)
		/ ((LONG)100)) + (LONG)(1 << 14))) >> 10;
	var3 = var1 * var2;
	var4 = par_h6 << 7;
	var4 = ((var4) + ((temp_scaled * par_h7) / ((LONG)100))) >> 4;
	var5 = ((var3 >> 14) * (var3 >> 14)) >> 10;
	var6 = (var4*var5)>>1;
	D(("var1 %ld var2 %ld var3 %ld var4 %ld var5 %ld var6 %ld\n",var1,var2,var3,var4,var5,var6));

#if 0
	/* this is in % * 100 */
	res = (((var3+var6)>>10)*(LONG)1000)>>12;
	Printf("Hum %ld\n",res);
#endif	
	/* this is in % * 65536 */
	res = ((var3+var6+32)>>6);

	if (res > 100*65536 ) /* Cap at 100%rH */
		res = 100*65536;
	if (res < 0)
		res = 0;

	return res;
}


/*
   PRIVATE
   custom temperature reading code for Bosch BMP280

*/
static LONG save_tempBMP280 = 25*256/5*100; /* def: 25�C room temperature (before first temp reading) */
LONG SAVEDS custom_BMP280T(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s)
{
	LONG  res;
	LONG  dig_T1,dig_T2,dig_T3,adc_T,var1,var2,T;
	UBYTE *p;
	
	D(("BMP280T\n"));

	/* get Temperature calibration */
	p    = (UBYTE*)&i2c_sensorbase->readbuffer[ 0 ];
	p[0] = 0x88;
	SendI2C( s->i2caddr, 1, p );

	res = ReceiveI2C( s->i2caddr, 6, p );
	if( !(res & 0xf) )
	{
		return	0;
	}

	/* temp calibration is little endian */
	dig_T1 = p[0] + ( (ULONG)p[1] << 8 );
	dig_T2 = p[2] + ( (LONG)(*((BYTE*)&p[3])) << 8 );
	dig_T3 = p[4] + ( (LONG)(*((BYTE*)&p[5])) << 8 );

	D(("dig_T1 %ld dig_T2 %ld dig_T3 %ld\n",dig_T1,dig_T2,dig_T3 ));

	p[0] = 0xFa;
	SendI2C( s->i2caddr, 1, p );

	res = ReceiveI2C( s->i2caddr, 3, p );
	if( !(res & 0xf) )
	{
		return	0;
	}

	adc_T = ((LONG)p[0] << 12) | ((LONG)p[1] << 4) | (p[2]>>4);

	D(("adc_T %ld\n",adc_T));
	var1 = SMult32( ( (adc_T>>3) - (dig_T1<<1) ), dig_T2 ) >> 11; /* var1 = ( ( (adc_T>>3) - (dig_T1<<1) ) * dig_T2 ) >> 11; */
	var2 = SMult32( (SMult32( ( (adc_T>>4) - dig_T1 )     , ( (adc_T>>4) - dig_T1 ) ) >> 12), dig_T3 ) >> 14; /* var2 = (((( (adc_T>>4) - dig_T1 )     * ( (adc_T>>4) - dig_T1 ) ) >>12)*dig_T3)>>14; */
	D(("var1 %ld var2 %ld ",var1,var2));

	/* T = ((var1 + var2) * 5 + 128)>>8; */ /* temp/�C*100 */

	save_tempBMP280 = var1+var2;

	T = SDivMod32( ((var1+var2)<<7), 10 );
	/* T = ((var1+var2)<<7)/10; */ /* temp/�C * 65536 */
        
	D(("result T 0x%lx\n",T));

	return T;
}



/*
   PRIVATE
   custom pressure reading code for Bosch BMP280

*/
LONG SAVEDS custom_BMP280P(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s)
{
	LONG var1,var2,var3,res,adc_P;
	LONG  dig_P1,dig_P2,dig_P3,dig_P4,dig_P5,dig_P6,dig_P7,dig_P8,dig_P9;
	ULONG cP;
	UBYTE *p;

	D(("BMP280P\n"));

	/* get Pressure calibration */
	p    = (UBYTE*)&i2c_sensorbase->readbuffer[ 0 ];
	p[0] = 0x8E;
	SendI2C( s->i2caddr, 1, p );

	res = ReceiveI2C( s->i2caddr, 18, p );
	if( !(res & 0xf) )
	{
		return	0;
	}

	/* temp calibration is little endian */
	dig_P1 = p[0]  + ( (ULONG)p[1] << 8 ); /* unsigned */
	dig_P2 = p[2]  + ( (LONG)(*((BYTE*)&p[3]))  << 8 );
	dig_P3 = p[4]  + ( (LONG)(*((BYTE*)&p[5]))  << 8 );
	dig_P4 = p[6]  + ( (LONG)(*((BYTE*)&p[7]))  << 8 );
	dig_P5 = p[8]  + ( (LONG)(*((BYTE*)&p[9]))  << 8 );
	dig_P6 = p[10] + ( (LONG)(*((BYTE*)&p[11])) << 8 );
	dig_P7 = p[12] + ( (LONG)(*((BYTE*)&p[13])) << 8 );
	dig_P8 = p[14] + ( (LONG)(*((BYTE*)&p[15])) << 8 );
	dig_P9 = p[16] + ( (LONG)(*((BYTE*)&p[17])) << 8 );
/*
	Printf("dig_P1 %ld dig_P2 %ld dig_P3 %ld dig_P4 %ld dig_P5 %ld dig_P6 %ld ",dig_P1,dig_P2,dig_P3,dig_P4,dig_P5,dig_P6 );
	Printf("dig_P7 %ld dig_P8 %ld dig_P9 %ld\n",dig_P7,dig_P8,dig_P9 );
*/
	/* get current pressure */
	p[0] = 0xF7;
	SendI2C( s->i2caddr, 1, p );

	res = ReceiveI2C( s->i2caddr, 3, p );
	if( !(res & 0xf) )
	{
		return	0;
	}
	adc_P = ((LONG)p[0] << 12) | ((LONG)p[1] << 4) | (p[2]>>4);

	D(("save_tempBMP280 %ld\n",save_tempBMP280));

	/* temperature compensation calculations */
	var1  = ( save_tempBMP280>>1 ) - 64000;
	var2  = SMult32( (SMult32( (var1>>2), (var1>>2) ) >>11), dig_P6 );
	var2 += SMult32( var1, dig_P5 ) << 1;
	var2  = (var2>>2)+(dig_P4<<16);
	
	var1  = (
	         (SMult32( dig_P3, ( 
	                            SMult32( var1>>2, var1>>2 ) >> 13 
	                           ) 
	         ) >> 3)
	         +
	         (SMult32( dig_P2, var1 )>>1)
	        )>>18;
	var1  =  SMult32( 32768+var1 , dig_P1 ) >> 15;
	if( !var1 )
		return 0;
		
	D(("var1 %ld var2 %ld\n",var1,var2));
	

	/* pressure computations */
	cP = UMult32( (1048576-adc_P) - (var2>>12) , 3125 );
	D(("cP %ld (0x%lx)\n",cP,cP));

	if( cP < 0x80000000 )
	 cP = UDivMod32( (cP<<1), var1 );
	else
	 cP = UDivMod32( cP, var1 ) << 1;

	var1 = (SMult32( dig_P9, ( SMult32( cP>>3, cP>>3 ) >> 13 ) ) + 2048 ) >> 12;
	var2 = (SMult32( (cP>>2), dig_P8 ) + 4096 ) >> 13; 

	cP  += ((var1 + var2 + dig_P7) >> 4);
	D(( (STRPTR)"Pressure %ld Pa\n",cP ));
#if 1
	/* convert Pa to Fix16.16 hPa                                    */
	/* separate by high part and low part and do some integer voodoo */
	var1 =  cP & 0x0000ffff;
	var2 = (cP & 0xffff0000)<<12; /* see below for additional <<4 */

	var3 = UDivMod32( UMult32( var1, 65536 )+50, 100 );
	var2 = UDivMod32( var2+50, 100 )<<4;

	cP = var2 + var3;
#else
	/* TODO: increase accuracy, we're losing a couple of bits here 
	         (currently direct application of Bosch formula + adjustment) */

	cP   = SMult32( cP, 655 ); /* convert Pa to Fix16.16 hPa */
#endif
	return (LONG)cP;
}

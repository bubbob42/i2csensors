#ifndef _LIBRARIES_I2CSENSORS_H
#define _LIBRARIES_I2CSENSORS_H

/*
;******************************************************************************************
;*
;* $id: i2csensors.h $
;*
;* Definitions for I2C Sensors Library interface
;*
;*
;* author: (C) 2018 Henryk Richter
;*
;******************************************************************************************
*/

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif	/* EXEC_TYPES_H */

#ifndef EXEC_LIBRARIES_H
#include <exec/libraries.h>
#endif	/* EXEC_LIBRARIES_H */

#ifndef _INC_I2CCLASS_H
#include <libraries/i2cclass.h>
#endif

/*
;*
;* Name
;*
*/
#define SENSORSNAME "i2csensors.library"


#endif	/* RESOURCES_VAMPIRE_H */

#ifndef _VBCCINLINE_I2CSENSORS_H
#define _VBCCINLINE_I2CSENSORS_H

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif

LONG __i2c_SensorNum(__reg("a6") void *, __reg("d0") LONG type)="\tjsr\t-30(a6)";
#define i2c_SensorNum(type) __i2c_SensorNum(I2CSensorsBase, (type))

LONG __i2c_ReadSensor(__reg("a6") void *, __reg("d0") LONG type, __reg("d1") LONG index, __reg("a1") BYTE ** unitstring, __reg("a2") BYTE ** namestring)="\tjsr\t-36(a6)";
#define i2c_ReadSensor(type, index, unitstring, namestring) __i2c_ReadSensor(I2CSensorsBase, (type), (index), (unitstring), (namestring))

LONG __i2c_Obtain(__reg("a6") void *, __reg("d0") LONG i2cAddress)="\tjsr\t-42(a6)";
#define i2c_Obtain(i2cAddress) __i2c_Obtain(I2CSensorsBase, (i2cAddress))

LONG __i2c_Release(__reg("a6") void *, __reg("d0") LONG i2cAddress)="\tjsr\t-48(a6)";
#define i2c_Release(i2cAddress) __i2c_Release(I2CSensorsBase, (i2cAddress))

ULONG __i2c_Send(__reg("a6") void *, __reg("d0") UBYTE i2caddr, __reg("d1") UWORD nbytes, __reg("a1") UBYTE * senddata)="\tjsr\t-54(a6)";
#define i2c_Send(i2caddr, nbytes, senddata) __i2c_Send(I2CSensorsBase, (i2caddr), (nbytes), (senddata))

ULONG __i2c_Receive(__reg("a6") void *, __reg("d0") UBYTE i2caddr, __reg("d1") UWORD nbytes, __reg("a1") UBYTE * recvdata)="\tjsr\t-60(a6)";
#define i2c_Receive(i2caddr, nbytes, recvdata) __i2c_Receive(I2CSensorsBase, (i2caddr), (nbytes), (recvdata))

ULONG __i2c_SensorID(__reg("a6") void *, __reg("d0") LONG type, __reg("d1") LONG index)="\tjsr\t-66(a6)";
#define i2c_SensorID(type, index) __i2c_SensorID(I2CSensorsBase, (type), (index))

LONG __i2c_SensorTypeFromID(__reg("a6") void *, __reg("d0") ULONG ID, __reg("a0") LONG * index)="\tjsr\t-72(a6)";
#define i2c_SensorTypeFromID(ID, index) __i2c_SensorTypeFromID(I2CSensorsBase, (ID), (index))

#endif /*  _VBCCINLINE_I2CSENSORS_H  */

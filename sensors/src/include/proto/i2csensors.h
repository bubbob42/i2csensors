/* Automatically generated header (sfdc 1.10)! Do not edit! */

#ifndef PROTO_I2CSENSORS_H
#define PROTO_I2CSENSORS_H

#include <clib/i2csensors_protos.h>

#ifndef _NO_INLINE
# if defined(__GNUC__)
#  ifdef __AROS__
#   include <defines/i2csensors.h>
#  else
#   include <inline/i2csensors.h>
#  endif
# else
#  include <pragmas/i2csensors_pragmas.h>
# endif
#endif /* _NO_INLINE */

#ifdef __amigaos4__
# include <interfaces/i2csensors.h>
# ifndef __NOGLOBALIFACE__
   extern struct I2CSensors IFace *II2CSensors ;
# endif /* __NOGLOBALIFACE__*/
#endif /* !__amigaos4__ */
#ifndef __NOLIBBASE__
  extern struct Library *
# ifdef __CONSTLIBBASEDECL__
   __CONSTLIBBASEDECL__
# endif /* __CONSTLIBBASEDECL__ */
  I2CSensorsBase ;
#endif /* !__NOLIBBASE__ */

#endif /* !PROTO_I2CSENSORS_H */

/* Automatically generated header (sfdc 1.10)! Do not edit! */

#ifndef _INLINE_I2CSENSORS_H
#define _INLINE_I2CSENSORS_H

#ifndef _SFDC_VARARG_DEFINED
#define _SFDC_VARARG_DEFINED
#ifdef __HAVE_IPTR_ATTR__
typedef APTR _sfdc_vararg __attribute__((iptr));
#else
typedef ULONG _sfdc_vararg;
#endif /* __HAVE_IPTR_ATTR__ */
#endif /* _SFDC_VARARG_DEFINED */

#ifndef __INLINE_MACROS_H
#include <inline/macros.h>
#endif /* !__INLINE_MACROS_H */

#ifndef I2CSENSORS_BASE_NAME
#define I2CSENSORS_BASE_NAME I2CSensorsBase 
#endif /* !I2CSENSORS_BASE_NAME */

#define i2c_SensorNum(___type) \
      LP1(0x1e, LONG, i2c_SensorNum , LONG, ___type, d0,\
      , I2CSENSORS_BASE_NAME)

#define i2c_ReadSensor(___type, ___index, ___unitstring, ___namestring) \
      LP4(0x24, LONG, i2c_ReadSensor , LONG, ___type, d0, LONG, ___index, d1, BYTE **, ___unitstring, a1, BYTE **, ___namestring, a2,\
      , I2CSENSORS_BASE_NAME)

#define i2c_Obtain(___i2cAddress) \
      LP1(0x2a, LONG, i2c_Obtain , LONG, ___i2cAddress, d0,\
      , I2CSENSORS_BASE_NAME)

#define i2c_Release(___i2cAddress) \
      LP1(0x30, LONG, i2c_Release , LONG, ___i2cAddress, d0,\
      , I2CSENSORS_BASE_NAME)

#define i2c_Send(___i2caddr, ___nbytes, ___senddata) \
      LP3(0x36, ULONG, i2c_Send , UBYTE, ___i2caddr, d0, UWORD, ___nbytes, d1, UBYTE *, ___senddata, a1,\
      , I2CSENSORS_BASE_NAME)

#define i2c_Receive(___i2caddr, ___nbytes, ___recvdata) \
      LP3(0x3c, ULONG, i2c_Receive , UBYTE, ___i2caddr, d0, UWORD, ___nbytes, d1, UBYTE *, ___recvdata, a1,\
      , I2CSENSORS_BASE_NAME)

#define i2c_SensorID(___type, ___index) \
      LP2(0x42, ULONG, i2c_SensorID , LONG, ___type, d0, LONG, ___index, d1,\
      , I2CSENSORS_BASE_NAME)

#define i2c_SensorTypeFromID(___ID, ___index) \
      LP2(0x48, LONG, i2c_SensorTypeFromID , ULONG, ___ID, d0, LONG *, ___index, a0,\
      , I2CSENSORS_BASE_NAME)

#endif /* !_INLINE_I2CSENSORS_H */

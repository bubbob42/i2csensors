/*
  file:   i2cclass_rtc.h

  proposal for I2C class by the example of RTC module support

  author: Henryk Richter <henryk.richter@gmx.net>

*/
#ifndef _INC_I2CCLASS_SENSOR_H
#define _INC_I2CCLASS_SENSOR_H

#ifndef _INC_I2CCLASS_H
#include <libraries/i2cclass.h> /* will load utility/tagitem.h */
#endif

#ifndef DOS_DOS_H
#include <dos/dos.h>
#endif
#ifndef EXEC_LIBRARIES_H
#include <exec/libraries.h>
#endif
#ifndef EXEC_SEMAPHORES_H
#include <exec/semaphores.h>
#endif

#include "compiler.h"

#ifdef _I2CCLASS_INTERNAL
/* redirect Library pointers */
#define SysBase i2c_sensorbase->sysbase
#define UtilityBase i2c_sensorbase->utilitylib
#define DOSBase i2c_sensorbase->doslib
#define I2C_Base i2c_sensorbase->i2clib
#define MathIeeeSingBasBase i2c_sensorbase->mathieeesingbas
/*#define TimerBase (struct Library *)i2c_sensorbase->timer.tr_node.io_Device*/
#endif

/* ------------------------------------------------------------------ */
/* ------------------------------------------------------------------ */
/* "I2C Sensor class base" (I2C_BASE+$1000), lowest would be 0x200    */
#define I2SEN_BASE I2C_BASE+0x1000
#define I2SEN_MEMHANDLE (I2SEN_BASE+0x80)    /* internal: memory handler */
#define I2SEN_CUSTOMBASE (I2SEN_MEMHANDLE+1) /* internal: designation for custom routines */

#define I2SEN_DEVICE    (I2SEN_BASE+1)    /* name string */
#define I2SEN_INIT      (I2SEN_BASE+2)    /* sensor global init sequence */
#define I2SEN_TYPE      (I2SEN_BASE+3)    /* sensor type (re-uses I2C_VOLTAGE etc. but here as ti_Data) -> starts an input source */
#define I2SEN_READPRE   (I2SEN_BASE+4)    /* preamble for sensor read (one transaction) */
#define I2SEN_RBYTES    (I2SEN_BASE+6)    /* number of bytes to read after sending preamble */
#define I2SEN_BITOFFSET (I2SEN_BASE+7)    /* bit offset of value to grab from bytes read */
#define I2SEN_BITS      (I2SEN_BASE+8)    /* number of bits to grab starting with the offset specified by I2SEN_BITOFFSET */
#define I2SEN_UNIT      (I2SEN_BASE+9)    /* Unit: V,A,W,°C,RPM,bar */
#define I2SEN_MULT      (I2SEN_BASE+10)   /* Multiplier Fix16.16 for range adjustment (def: 1.0) */
#define I2SEN_OFFSET    (I2SEN_BASE+11)   /* Additive offset (def:0), final_value = (RAW_READ+OFFSET)*MULT */
#define I2SEN_SIGN_BITOFF (I2SEN_BASE+12) /* bit offset for sign (if applicable) */
#define I2SEN_CUSTOM    (I2SEN_BASE+13)   /* custom processing, see below */
#define I2SEN_EXTRMASK  (I2SEN_BASE+14)   /* bit extract mask (def: 0xffffffff, i.e. use all bits) */
#define I2SEN_WAKEUP    (I2SEN_BASE+15)   /* run a "wakeup" cycle before accessing sensor (chip select/deselect) */
#define I2SEN_DIVBY     (I2SEN_BASE+16)   /* replace I2SEN_MULT style MUL*val by MUL/val, if !=0 */

/* ------------------------------------------------------------------ */
/* ------------------------------------------------------------------ */
/* custom processing tags                                             */
#define I2SENC_BMP280T  (I2SEN_CUSTOMBASE+1) /* BMP280 Temp     */
#define I2SENC_BMP280P  (I2SEN_CUSTOMBASE+2) /* BMP280 Pressure */
#define I2SENC_BME680T  (I2SEN_CUSTOMBASE+3) /* BME680 Temp     */
#define I2SENC_BME680P  (I2SEN_CUSTOMBASE+4) /* BME680 Pressure */
#define I2SENC_BME680H  (I2SEN_CUSTOMBASE+5) /* BME680 Humidity */
#define I2SENC_BME680G  (I2SEN_CUSTOMBASE+6) /* BME680 Gas resistance */
#define I2SENC_TK60T    (I2SEN_CUSTOMBASE+7) /* Matze TK60 CPU temperature */


/* ------------------------------------------------------------------ */
/* ------------------------------------------------------------------ */
/* structures                                                         */
/* ------------------------------------------------------------------ */
struct i2c_sensorbase {
	struct Library libnode;
	BPTR   seglist;
	struct SignalSemaphore LockSemaphore;
	/* */
	struct Library *i2clib;
	struct Library *sysbase; /* execbase must be at struct+4 for old/disabled SensorOpen() */
	struct Library *doslib;
	struct Library *utilitylib;
	struct Library *mathieeesingbas;
	struct TagItem *deviceconfig; /* supported chips -> taglist that may be handed out */
	struct List sensors;          /* active sensors (internal structure)               */
	UBYTE  readbuffer[256];       /* should be enough (TM) */
/*	struct timerequest timer;*/
};
#define I2C_SENSORBASE_DECL


/* ------------------------------------------------------------------ */
/* Functions                                                          */
/* ------------------------------------------------------------------ */
/*
   concept: classic open() / do_something() / close()

   weak points:
*/
ASM LONG            LibNull(           void                                        );
ASM struct Library *LibInit(           ASMR(d0) struct i2c_sensorbase * ASMREG(d0),
                                       ASMR(a0) BPTR                    ASMREG(a0),
                                       ASMR(a6) struct Library *        ASMREG(a6) );
ASM SAVEDS struct Library *LibOpen(    ASMR(a6) struct i2c_sensorbase * ASMREG(a6) );			   
ASM BPTR                   LibClose(   ASMR(a6) struct i2c_sensorbase * ASMREG(a6) );			   
ASM BPTR                   LibExpunge( ASMR(a6) struct i2c_sensorbase * ASMREG(a6) );

/* main API */
ASM LONG i2c_SensorClose( ASMR(a6) struct i2c_sensorbase * ASMREG(a6) );

/* retrieve number of inputs for a specific type 
   I2C_VOLTAGE,I2C_CURRENT,I2C_TEMP,I2C_FAN,I2C_PRESSURE,...
*/
ASM LONG i2c_SensorNum( ASMR(a6) struct i2c_sensorbase * ASMREG(a6), 
                        ASMR(d0) LONG ASMREG(d0) );

/* get an ID for a specific sensor that is supposed to stay
   constant across reboots.
*/
ASM ULONG i2c_SensorID( ASMR(a6) struct i2c_sensorbase * ASMREG(a6), 
                        ASMR(d0) LONG ASMREG(d0), 
                        ASMR(d1) LONG ASMREG(d1) );

/* get type and index for a given ID
*/
ASM LONG i2c_SensorTypeFromID( ASMR(a6) struct i2c_sensorbase * ASMREG(a6), 
                               ASMR(d0) ULONG ASMREG(d0), 
                               ASMR(a0) LONG* ASMREG(a0) );

/*
  read from a sensor and return reading as well as sensor name/title string
  and measurement unit string
*/
ASM LONG i2c_ReadSensor( ASMR(a6) struct i2c_sensorbase * ASMREG(a6),
                         ASMR(d0) LONG                    ASMREG(d0),
                         ASMR(d1) LONG                    ASMREG(d1),
                         ASMR(a1) BYTE **                 ASMREG(a1),
                         ASMR(a2) BYTE **                 ASMREG(a2) );

/* 
   delegates for functions you'll find in i2c.library, with a twist here:

   there is the i2c_Obtain()/i2c_Release() pair to arbitrate bus access,
   which helps avoid command collisions in cases where read/write to the
   same address is desired.

   i2c_Send(), i2c_Receive() are just pass-through calls to i2c.library
*/
ASM LONG i2c_Obtain(  ASMR(a6) struct i2c_sensorbase * ASMREG(a6),
                      ASMR(d0) LONG                    ASMREG(d0)
                   );
ASM LONG i2c_Release( ASMR(a6) struct i2c_sensorbase * ASMREG(a6), 
                      ASMR(d0) LONG                    ASMREG(d0)
                    );
ASM LONG i2c_Send(    ASMR(a6) struct i2c_sensorbase * ASMREG(a6),
                      ASMR(d0) UBYTE                   ASMREG(d0),
                      ASMR(d1) UWORD                   ASMREG(d1),
                      ASMR(a1) UBYTE *                 ASMREG(a1)
                 );
ASM LONG i2c_Receive( ASMR(a6) struct i2c_sensorbase * ASMREG(a6), 
                      ASMR(d0) UBYTE                   ASMREG(d0),
                      ASMR(d1) UWORD                   ASMREG(d1),
                      ASMR(a1) UBYTE *                 ASMREG(a1)
                    );


#endif /* _INC_I2CCLASS_SENSOR_H */

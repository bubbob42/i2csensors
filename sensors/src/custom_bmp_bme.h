/*
  file:   custom_bmp_bme.h

  proposal for I2C class for sensor reading

  author:  Henryk Richter <henryk.richter@gmx.net>

  concept: This file contains the sensor reading and
           parsing functions for Bosch sensors,
           beginning with the BMP280 and BME680.
           
           Custom sensors are defined in three places:
           i2cclass_sensor.h
            definition of custom sensor Tag name
             I2SENC_*
           config.c
            definition of keyword to Tag name
             struct keytag confcustomtag[] = {}
           i2cclass_sensor.c
            function call, see "case I2SEN_CUSTOM:"
  
*/
#ifndef _INC_CUSTOM_BMP_BME_H
#define _INC_CUSTOM_BMP_BME_H

#include "macros.h"
#include "config.h"
#include "i2cclass_sensor.h"
#include "debug.h"

LONG SAVEDS custom_BME680T(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s);
LONG SAVEDS custom_BME680P(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s);
LONG SAVEDS custom_BME680H(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s);
LONG SAVEDS custom_BME680G(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s);

LONG SAVEDS custom_BMP280T(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s);
LONG SAVEDS custom_BMP280P(struct i2c_sensorbase *i2c_sensorbase, struct i2s_sensor*s);



#endif /* _INC_CUSTOM_BMP_BME_H */

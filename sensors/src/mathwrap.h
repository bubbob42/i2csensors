/*
  mathwrap.h

  (C) 2018 Henryk Richter <henryk.richter@gmx.net>

  simple math functions, either calling mathieeesingbas.library or assuming FPU presence 

  syntax:

*/
#ifndef _INC_MATHWRAP_H
#define _INC_MATHWRAP_H

/* TODO: pass argument via compiler cmdline whether FPU is present or not
   USE_MATHLIB, when defined will trigger explicit MathIEEESingBas
*/
/* I`ve had problems (multiply) with mathieeesingbas, disabled USE_MATHLIB */
/* #define USE_MATHLIB */

#ifdef USE_MATHLIB

/*#include <libraries/mathieeesp.h>*/
#include <proto/mathieeesingbas.h>
#define FRACNUM FLOAT
#define FADD( a, b ) IEEESPAdd( a, b )
#define FMUL( a, b ) IEEESPMul( a, b )
#define FSUB( a, b ) IEEESPSub( a, b )
#define FDIV( a, b ) IEEESPDiv( a, b )
#define FLT( a )    IEEESPFlt( a )
#define FIX( a )    IEEESPFix( a )

#else /* USE_MATHLIB */

/* please note that the constructs below quite often require 
   an FPU when a .library without startup code is built */
#ifdef   USE_MATH
#include <math.h>
#define  FRACNUM float

#define FADD( a, b ) ( (a) + (b) )
#define FMUL( a, b ) ( (a) * (b) )
#define FSUB( a, b ) ( (a) - (b) )
#define FDIV( a, b ) ( (a) / (b) )
#define FLT( a )    (FRACNUM)(a)
#define FIX( a )    (LONG)(a)

#else

#define  FRACNUM LONG

#endif

#endif /* USE_MATHLIB */


#endif /* _INC_MATHWRAP_H */

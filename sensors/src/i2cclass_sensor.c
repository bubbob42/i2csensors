/*
  file:   i2cclass_sensor.c

  proposal for I2C class for sensor reading

  author:  Henryk Richter <henryk.richter@gmx.net>

  concept: I2C sensors are in multiple ways everything
           but unique in their appearance on the bus.
	   Not only do several chips carry the same I2C
	   address but many of them can also be configured 
	   to carry out different functions.

	   As a consequence, this sensor class will not
	   attempt to provide its functionality in a 
	   hardcoded manner. The resulting code is accompanied
	   by a text configuration file that allows the
	   user modifications to the set of supported
	   chips and their functionality as needed.

           No rules without exceptions, though. Some chips
	   for whom the linear model is not sufficent in terms
	   of computations can be supported explicitly in here.
	   Steps:
	    - define a new type I2SENC_ in i2cclass_sensor.h
	    - define a new keytag for the confcustomtag table
	      in config.c
	    - provide a custom processing routine here and
	      add the function pointer below for I2SEN_CUSTOM
	      in i2c_SensorTranslateConfig 

  
*/
#include <exec/memory.h>
#include <proto/exec.h>
#include <proto/utility.h>
#include <proto/dos.h>
#include <proto/timer.h>
#include <dos/exall.h>

#define __CONSTLIBBASEDECL__ /* some GCC targeted includes carry this attribute */ 
#include <proto/i2c.h>

#define _I2CCLASS_INTERNAL /* triggers the use of internal library bases */
#include "macros.h"
#include "config.h"
#include "i2cclass_sensor.h"
#include "debug.h"

#include "i2cclass_sensor_private.h"

/* custom functions */
#include "custom_bmp_bme.h"
#include "custom_tk60t.h"

/* local proto */
static LONG i2c_SensorTranslateConfig( struct i2c_sensorbase *i2c_sensorbase );
static LONG i2c_SensorInit( struct i2c_sensorbase *i2c_sensorbase );
ASM static LONG i2c_SensorConfigure( ASMR(a6) BASETYPE * ASMREG(a6) );

void StringCopy( STRPTR d, STRPTR s )
{
	while( *s )
	{
		*d++ = *s++;
	}
	*d = 0;
}

void StringCat( STRPTR d, STRPTR s )
{
	while( *++d ){}

	while( *s )
	{
		*d++ = *s++;
	}
	*d = 0;
}



/*

   Library init call

*/
ASM SAVEDS struct Library *LibInit( ASMR(d0) BASETYPE *i2c_sensorbase  ASMREG(d0),
                              ASMR(a0) BPTR seglist              ASMREG(a0),
                              ASMR(a6) struct Library *_SysBase  ASMREG(a6) )
{
	UBYTE *p = (UBYTE*)i2c_sensorbase;
	extern char *_LibVersionString;

	if( !p )
		return (0);

	MemClear( p + sizeof(struct Library), sizeof(struct i2c_sensorbase) - sizeof(struct Library) );

	SysBase = *((struct Library **)4UL); /* copy ExecBase ptr to new struct */

	D(("Before Libs open\n"));

	InitSemaphore( &i2c_sensorbase->LockSemaphore );

	i2c_sensorbase->seglist    = seglist;
	i2c_sensorbase->libnode.lib_IdString = _LibVersionString;

	/* config.c uses memory pools, require kick 3.0 */
	i2c_sensorbase->i2clib     = OpenLibrary( (STRPTR)"i2c.library", 39     );
	i2c_sensorbase->utilitylib = OpenLibrary( (STRPTR)"utility.library", 39 );
	i2c_sensorbase->doslib     = OpenLibrary( (STRPTR)"dos.library", 37     );
	i2c_sensorbase->mathieeesingbas = OpenLibrary( (STRPTR)"mathieeesingbas.library",37);

	NEWLIST( &i2c_sensorbase->sensors );

	D(("After Libs open\n"));

	if( (!i2c_sensorbase->doslib)     || 
	    (!i2c_sensorbase->i2clib)     ||
	    (!i2c_sensorbase->utilitylib) ||
	    (!i2c_sensorbase->mathieeesingbas )
	  )
	{
		if(i2c_sensorbase->doslib)
			Printf( (STRPTR)"Cannot open i2c.library V39\n");
		if( _SysBase ) /* no sysbase passed ? won`t happen in real life */
			i2c_SensorClose( i2c_sensorbase );
		return (0);
	}

	/* read config file(s) and parse them */
	i2c_SensorConfigure( i2c_sensorbase );

	D(("After SensorConfigure\n"));

#if 0
	/* read config */
	if( !(i2c_sensorbase->deviceconfig = config_parse(BASEARG (BYTE*)"test.cfg",(0)) ))
	{
		Printf( (STRPTR)"no config\n");
		i2c_SensorClose( i2c_sensorbase );
		return (0);
	}

	/* translate config tags into internal struct 
	   less flexible but faster (and private, too)
	*/
	i2c_SensorTranslateConfig( i2c_sensorbase );

	/* initialize all sensors */
	i2c_SensorInit( i2c_sensorbase );
#endif

	return (struct Library *)i2c_sensorbase;
}



/*

   Library open Call

*/
ASM SAVEDS struct Library * LibOpen( ASMR(a6) struct i2c_sensorbase *i2c_sensorbase ASMREG(a6) )
{
	ObtainSemaphore( &i2c_sensorbase->LockSemaphore );

	i2c_sensorbase->libnode.lib_Flags &= ~LIBF_DELEXP;
	i2c_sensorbase->libnode.lib_OpenCnt++;

	ReleaseSemaphore( &i2c_sensorbase->LockSemaphore );

	return (struct Library*)i2c_sensorbase;
}



/*

   Library close Call

*/
ASM SAVEDS BPTR LibClose( ASMR(a6) struct i2c_sensorbase *i2c_sensorbase ASMREG(a6) )
{

	ObtainSemaphore( &i2c_sensorbase->LockSemaphore );

	if( i2c_sensorbase->libnode.lib_OpenCnt > 0 )
		i2c_sensorbase->libnode.lib_OpenCnt--;

	ReleaseSemaphore( &i2c_sensorbase->LockSemaphore );

	if( (i2c_sensorbase->libnode.lib_Flags & LIBF_DELEXP) &&
	    (i2c_sensorbase->libnode.lib_OpenCnt == 0)
	  )
		return LibExpunge( i2c_sensorbase );

	return (0);
}



/*

   Library cleanup 

*/
ASM SAVEDS BPTR LibExpunge( ASMR(a6) struct i2c_sensorbase *i2c_sensorbase ASMREG(a6) )
{
	BPTR ret;

	if( i2c_sensorbase->libnode.lib_OpenCnt != 0 )
	{
		i2c_sensorbase->libnode.lib_Flags |= LIBF_DELEXP;
		return(0);
	}

	Forbid();

	REMOVE( i2c_sensorbase ); /* Lib no longer in system */

	Permit();

	ret = i2c_sensorbase->seglist;

	i2c_SensorClose( i2c_sensorbase );

	FreeMem( (APTR)( (ULONG)i2c_sensorbase - (ULONG)(i2c_sensorbase->libnode.lib_NegSize) ),
	         i2c_sensorbase->libnode.lib_NegSize + i2c_sensorbase->libnode.lib_PosSize );
	
	return ret;
}



#if 0
#if __GNUC__ > 5 
	extern struct i2c_sensorbase *GetEmptyBase();
#endif
/*
THIS REQUIRES A struct i2c_sensorbase looking like this
 struct i2c_sensorbase {
  APTR blah;
  struct ExecBase *sysbase;
  ...
 };

Current struct was remodeled to have a struct Library on top!
*/
/*-----------------------------------------------------------------------*/
/* TEMPORARY: init everything at "open" request, this will move to       */
/*            Library init phase                                         */
/*-----------------------------------------------------------------------*/

ASM struct i2c_sensorbase *i2c_SensorOpen( void )
{
#if __GNUC__ > 5 
	/* This is weird. With GCC 6.4.1b (20180401), the regular code assigning
	   0UL to sensorbase results in move.l 4.w,D0, followed by TRAP #7 
	   Wow. Kludge below.
	*/
	struct i2c_sensorbase *i2c_sensorbase = GetEmptyBase();
#else
	struct i2c_sensorbase *i2c_sensorbase = (struct i2c_sensorbase*)0UL;
#endif

	i2c_sensorbase = (struct i2c_sensorbase*)AllocMem( sizeof( struct i2c_sensorbase ), MEMF_PUBLIC|MEMF_CLEAR );
	if( !i2c_sensorbase )
		return (0);

	LibInit( i2c_sensorbase, (0), *( (struct Library**)4UL ) );

	return i2c_sensorbase;
}
#endif


/*

  Global Cleanup call right before the library is expunged
  (private)

*/
ASM LONG i2c_SensorClose( ASMR(a6) struct i2c_sensorbase *i2c_sensorbase ASMREG(a6) )
{
	if( !i2c_sensorbase )
		return I2C_ERR_BADARG;

	{
	 struct i2s_sensor *s; 

	 while( (s = (struct i2s_sensor*)GetHead( &i2c_sensorbase->sensors ) ))
	 {
		REMOVE(s);
		FreeVec(s);
	 }
	}

	if( i2c_sensorbase->deviceconfig )
		config_free( BASEARG i2c_sensorbase->deviceconfig );

	if( i2c_sensorbase->mathieeesingbas )
		CloseLibrary( i2c_sensorbase->mathieeesingbas );

	if( i2c_sensorbase->utilitylib)
		CloseLibrary( i2c_sensorbase->utilitylib );

	if( i2c_sensorbase->i2clib )
		CloseLibrary( i2c_sensorbase->i2clib );

	if( i2c_sensorbase->doslib )
		CloseLibrary( i2c_sensorbase->doslib );

	/* FreeMem( i2c_sensorbase, sizeof( struct i2c_sensorbase ) ); */ /* old test build */

	return I2C_OK;
}



ASM LONG i2c_Obtain(  ASMR(a6) struct i2c_sensorbase *i2c_sensorbase ASMREG(a6),
                      ASMR(d0) LONG i2cAddress                       ASMREG(d0) )
{
	/* only a single lock is present at the moment, ignore i2cAddress */
	ObtainSemaphore( &i2c_sensorbase->LockSemaphore );
	return i2cAddress;
}



ASM LONG i2c_Release( ASMR(a6) struct i2c_sensorbase *i2c_sensorbase ASMREG(a6),
                      ASMR(d0) LONG i2cAddress                       ASMREG(d0) )
{
	/* only a single lock is present at the moment, ignore i2cAddress */
	ReleaseSemaphore( &i2c_sensorbase->LockSemaphore );
	return 1;
}



ASM SAVEDS LONG i2c_Send(    ASMR(a6) struct i2c_sensorbase *i2c_sensorbase ASMREG(a6),
                      ASMR(d0) UBYTE  i2caddr                        ASMREG(d0),
                      ASMR(d1) UWORD  nbytes                         ASMREG(d1),
                      ASMR(a1) UBYTE *ptr                            ASMREG(a1)
                 )
{
	return (LONG)SendI2C( i2caddr, nbytes, ptr );
}



ASM SAVEDS LONG i2c_Receive( ASMR(a6) struct i2c_sensorbase *i2c_sensorbase ASMREG(a6), 
                      ASMR(d0) UBYTE  i2caddr                  ASMREG(d0),
                      ASMR(d1) UWORD  nbytes                   ASMREG(d1),
                      ASMR(a1) UBYTE *ptr                      ASMREG(a1)
                    )
{
	return (LONG)ReceiveI2C( i2caddr, nbytes, ptr );
}



/*

  Public: return the number of sensors of a given type

*/
ASM LONG i2c_SensorNum( ASMR(a6) struct i2c_sensorbase *i2c_sensorbase ASMREG(a6),
                        ASMR(d0) LONG type ASMREG(d0) )
{
	LONG ret = 0;
	/*struct TagItem *conf,*curdev;*/
	struct i2s_sensor *s;

	if( !i2c_sensorbase )
		return ret;

#if 1

	/* quicker search through linked list */
	s=(struct i2s_sensor*)i2c_sensorbase->sensors.lh_Head;
	while( s->n.mln_Succ )
	{
		if( s->type == type )
			ret++;

		s=(struct i2s_sensor*)s->n.mln_Succ;
	}

#else

	/* old manual search through taglists, should still work, I hope */
	if( !(conf=i2c_sensorbase->deviceconfig) )
		return ret;

	/* two-level taglist: device, attributes by device */
	while( (curdev = NextTagItem(&conf)) )
	{
		/* this is a device entry */
		if( curdev->ti_Tag == I2C_DEVENTRY )
		{
			curdev = (struct TagItem*)curdev->ti_Data;
			/* we have a device */
			while( (curdev=FindTagItem(I2SEN_TYPE,curdev) ))
			{
				if( curdev->ti_Data == type )
					ret++;
			}
		}
	}

#endif

	return ret;

}

/* get an ID for a specific sensor that is supposed to stay
   constant across reboots.
*/
ASM ULONG i2c_SensorID( ASMR(a6) struct i2c_sensorbase *i2c_sensorbase ASMREG(a6), 
                        ASMR(d0) LONG type  ASMREG(d0),
			ASMR(d1) LONG index ASMREG(d1) )
{
	LONG   count=0;
	struct i2s_sensor *s;

	if( !i2c_sensorbase )
		return 0;

	/* search through list */
	s=(struct i2s_sensor*)i2c_sensorbase->sensors.lh_Head;
	while( s->n.mln_Succ )
	{
		if( s->type == type )
		{
			if( index == count )
				break;
			count++;
		}

		s=(struct i2s_sensor*)s->n.mln_Succ;
	}
	if( !s->n.mln_Succ )
		return 0;	/* wrong index */
#if 1
	return s->id;
#else
	{
	 LONG   res;
	 UBYTE  *p;

	 /* compute something out of the I2C Address, Index and the sensor name */
	 res = 0;
	 if( (p=s->name) )
	 {
		while( *p )
			res = ((res^0x7f3e)>>1) + (((ULONG)*p++)<<8);
	 }
	 res |= ((ULONG)(s->i2caddr))<<24;
	 res |= index<<20;
	 return res;
	}
#endif
}

/* get type and index for a given ID
*/
ASM LONG i2c_SensorTypeFromID( ASMR(a6) struct i2c_sensorbase * i2c_sensorbase ASMREG(a6), 
                               ASMR(d0) ULONG ID    ASMREG(d0), 
                               ASMR(a0) LONG* index ASMREG(a0) )
{
	LONG   res,count=0;
	struct i2s_sensor *s;

	if( !i2c_sensorbase )
		return 0;

	/* search through list */
	s=(struct i2s_sensor*)i2c_sensorbase->sensors.lh_Head;
	while( s->n.mln_Succ )
	{
		if( s->id == ID )
			break;
		s=(struct i2s_sensor*)s->n.mln_Succ;
	}
	if( !s->n.mln_Succ )
		return 0;	/* id not found */

	res = s->type;

	if( index )
	{
		s=(struct i2s_sensor*)i2c_sensorbase->sensors.lh_Head;
		while( s->n.mln_Succ )
		{
			if( s->id == ID )
				break;
			if( s->type == res )
			{
				count++;
			}
			s=(struct i2s_sensor*)s->n.mln_Succ;
		}
		*index = count;
	}
	
	return res;
}


/*

  Public: read sensor and return it's value

*/
ASM SAVEDS LONG i2c_ReadSensor( ASMR(a6) struct i2c_sensorbase *i2c_sensorbase ASMREG(a6),
                         ASMR(d0) LONG type         ASMREG(d0),
			 ASMR(d1) LONG index        ASMREG(d1),
			 ASMR(a1) BYTE **unitstring ASMREG(a1),
			 ASMR(a2) BYTE **namestring ASMREG(a2) )
{
	LONG   res,t,sval=0,count=0;
	ULONG  val;
	UBYTE  *p;
	struct i2s_sensor *s;

	if( !i2c_sensorbase )
		return 0;


	/* search through list */
	s=(struct i2s_sensor*)i2c_sensorbase->sensors.lh_Head;
	if( type == I2C_ID )
	{
		while( s->n.mln_Succ )
		{
			if( s->id == (ULONG)index )
				break;
			s=(struct i2s_sensor*)s->n.mln_Succ;
		}
	}
	else
	{
		while( s->n.mln_Succ )
		{
			if( s->type == type )
			{
				if( index == count )
					break;
				count++;
			}
			s=(struct i2s_sensor*)s->n.mln_Succ;
		}
	}
	*unitstring = (0); /* def: error */
	*namestring = (0);
	
	if( !s->n.mln_Succ )
		return 0;	/* wrong index */

	ObtainSemaphore( &i2c_sensorbase->LockSemaphore );

	/* now we have the sensor struct to read from */
	if( s->cfunc )
	{
		if( !(sval = s->cfunc( i2c_sensorbase, s ) ))
			goto fail;
		goto end;
	}

	if( s->wakeup )
	{
	 BYTE dummy=0;
	 ReceiveI2C( s->i2caddr, 1, (BYTE*)&dummy);
         SendI2C(    s->i2caddr, 0, (BYTE*)&dummy);
	}


	/* preamble */
	if( (p=s->readpre) )
	{
		if(0){
			int i;
			Printf( (STRPTR)"preamble ");
			for(i=0; i<*p ; i++ )
			{
				Printf( (STRPTR)"%02lx ",p[1+i]);
			}
			Printf( (STRPTR)"\n");
		}
		SendI2C( s->i2caddr, *p, p+1 );
	}

	/* then read */
	res = ReceiveI2C( s->i2caddr, s->readbytes, i2c_sensorbase->readbuffer );
	if( !(res & 0xf) )
	{
		goto fail;
	}
	count = ( s->numbits ) ? s->numbits : ((LONG)s->readbytes)<<3;

/* #undef D */
/* #define D(x) Printf x */

	D(("Bytes %02lx %02lx %02lx %02lx ",(ULONG)i2c_sensorbase->readbuffer[0],
	                                    (ULONG)i2c_sensorbase->readbuffer[1],
	                                    (ULONG)i2c_sensorbase->readbuffer[2],
	                                    (ULONG)i2c_sensorbase->readbuffer[3] ));

	/* fetch up to 32 bits (left aligned) */
	val  = ((ULONG)i2c_sensorbase->readbuffer[ ((ULONG)(s->bitoffset)>>3)   ])<<24;
	val |= ((ULONG)i2c_sensorbase->readbuffer[ ((ULONG)(s->bitoffset)>>3)+1 ])<<16;
	val |= ((ULONG)i2c_sensorbase->readbuffer[ ((ULONG)(s->bitoffset)>>3)+2 ])<<8;
	val |= ((ULONG)i2c_sensorbase->readbuffer[ ((ULONG)(s->bitoffset)>>3)+3 ]);
	val <<= (s->bitoffset&7);
	val |= ((ULONG)i2c_sensorbase->readbuffer[ ((ULONG)(s->bitoffset)>>3)+4 ])>>(8-(s->bitoffset&7));

	/* extract relevant bits (right align) */
	val  >>= ( 32 - count );
	sval =   (LONG)val;

	/*Printf("SVAL %ld (0x%lx) ",sval,sval);*/

#if 1
	/* apply EXTRACT mask -> compact bits */
	if( s->extractmask != 0xffffffff )
	{
	 ULONG tval = sval;
	 ULONG msk = s->extractmask; /* right aligned */
	 ULONG nb;

	 for( nb = 0, sval = 0; nb < 32 ; nb++ )
	 {
		if( msk & 0x80000000 )
		{
			sval <<= 1;
			sval  |= (tval>>31);
		}
		else
			count--;
		msk  <<= 1;
		tval <<= 1;
	 }
	}
#endif

	/* is the reading a signed one ? */
	if( s->signbit >= 0 )
	{
	 	t  = i2c_sensorbase->readbuffer[ ((ULONG)s->signbit)>>3 ];
		t  = ( t >> (7-(s->signbit & 7))) & 1;
		if( t )
		{
			t = 0xFFFFFFFF<<(count);
			sval |= t;
			/* sval = -sval; */
		}
	}
#if 1 
#ifdef USE_MATH
	{
	 FRACNUM fval = FLT(sval); /* (float)sval; */

#if 0 /* def DEBUG */
	 Printf("RAW %ld (0x%lx) float ",sval,sval);
	 PrintFloat(BASEARG  fval );
#endif	/* DEBUG */

	 if( s->add.l )
	 {
		fval = FADD( fval, s->add.f );  /* fval += s->add.f; */
	 }
	 if( s->usedivby )
	 {
	 	fval = FDIV( s->mult.f, fval );
	 }
	 else
	 {
	 	if( s->mult.l )
	 	{
			fval = FMUL( fval, s->mult.f ); /* fval *= s->mult.f; */
		}
	 }
#if 0 /* def DEBUG */
	 Printf("after adjust ");
	 PrintFloat(BASEARG  fval );
#endif /* DEBUG */

	 sval = FIX( FADD( FMUL( fval, 65536.f ), 0.5f )); /* sval = (LONG)(fval * 65536. + 0.5);*/ /* convert to FIX1616 */
	 
	 D(("16.16 %ld = %lx.%04lx (hex)\n",sval,sval>>16,sval&65535));
	}
#else  /* USE_MATH */
	{ /* note: 020+ only, otherwise resort to utility.library */
	 unsigned long long t;

	 sval <<= 16;         /* FIX1616            */
	 sval += s->add.l;    /* add FIX1616 bias   */

	 if( s->usedivby )
	 	t = (sval != 0) ? s->mult.l / sval : 0;
	 else
		t = sval * s->mult.l; /* multiply to 64 bit */
	 sval = t>>32;
	}
#endif /* USE_MATH */
#endif /* if 1 */

end:
	*unitstring = (BYTE*)s->unit;
	*namestring = (BYTE*)s->name;
fail:
	ReleaseSemaphore( &i2c_sensorbase->LockSemaphore );

	return sval;

}




/* initialize all sensors */
static LONG i2c_SensorInit( struct i2c_sensorbase *i2c_sensorbase )
{
	struct TagItem *conf,*curdev,*tmp;
	ULONG  curaddr;
	UBYTE  *p;

	if( !i2c_sensorbase )
		return 0;
	if( !(conf=i2c_sensorbase->deviceconfig) )
		return 0;

	/* two-level taglist: device, attributes by device */
	while( (curdev = NextTagItem(&conf)) )
	{
		D(("---- Init Loop TAG %lx ----\n",curdev->ti_Tag));
		/* this is a device entry */
		if( curdev->ti_Tag == I2C_DEVENTRY )
		{
			curdev  = (struct TagItem*)curdev->ti_Data;
		
			tmp = FindTagItem(I2C_WRID,curdev);
			if( !tmp )
				continue;
			curaddr = tmp->ti_Data;

			D(("Addr %lx\n",curaddr));

			tmp = FindTagItem(I2SEN_INIT,curdev);
			if( !tmp )
				continue;
			p = (UBYTE*)tmp->ti_Data;
			
			D(("Init Length %ld\n",*p));

			SendI2C( curaddr, *p, p+1 );
		}
	}

	return 1;
}



/* 
   convert config tags into internal struct 
   less flexible but faster (and private, too)
*/
static LONG i2c_SensorTranslateConfig( struct i2c_sensorbase *i2c_sensorbase )
{
	struct TagItem *conf,*curdev,*tmp,*curtag;
	BYTE   *curname;
	ULONG   curaddr;
	struct i2s_sensor *s; /* = (0); */

	if( !i2c_sensorbase )
		return 0;
	if( !(conf=i2c_sensorbase->deviceconfig) )
		return 0;

	/* two-level taglist: device, attributes by device */
	while( (curdev = NextTagItem(&conf)) )
	{
		D(("---- TAG %lx ----\n",curdev->ti_Tag));
		/* this is a device entry */
		if( curdev->ti_Tag == I2C_DEVENTRY )
		{
			curdev  = (struct TagItem*)curdev->ti_Data;

			/* cache name and I2C address (global) across all sensors */
			curname = (BYTE*)"UNNAMED";
			tmp = FindTagItem(I2SEN_DEVICE,curdev); 
			if( tmp )
				curname = (BYTE*)tmp->ti_Data;

			tmp = FindTagItem(I2C_WRID,curdev);
			curaddr = 0;
			if( tmp )
				curaddr = tmp->ti_Data;

			s = (0);
			while( (curtag = NextTagItem(&curdev)) )
			{
				D(("ti_Tag %lx\n",curtag->ti_Tag ));
				if( curtag->ti_Tag == I2SEN_TYPE )
				{
					if( (s=(struct i2s_sensor*)AllocVec( sizeof(struct i2s_sensor), MEMF_PUBLIC|MEMF_CLEAR )))
					{
						s->i2caddr = curaddr;
						s->name    = curname;
						s->type    = curtag->ti_Data;
						s->signbit = -1;
						s->extractmask = 0xffffffff;
						s->wakeup  = 0;
					 	s->usedivby = 0;
						ADDTAIL( &i2c_sensorbase->sensors, s );
						D(("new type %lx\n",curtag->ti_Data));
					}
				}

				if(!s)
					continue;
				switch( curtag->ti_Tag )
				{
				 case I2SEN_READPRE:
				 	s->readpre = (UBYTE*)curtag->ti_Data;
				 	break;
				 case I2SEN_RBYTES:
				 	s->readbytes = curtag->ti_Data;
				 	break;
				 case I2SEN_BITOFFSET:
				 	s->bitoffset = curtag->ti_Data;
				 	break;
				 case I2SEN_BITS:
				 	s->numbits = curtag->ti_Data;
				 	break;
				 case I2SEN_UNIT:
					s->unit = (BYTE*)curtag->ti_Data;
					/*Printf("Unit %s\n",s->unit);*/
				 	break;
				 case I2SEN_MULT:
				 	s->mult.l = curtag->ti_Data; /* floats are also copied */
				 	break;
				 case I2SEN_OFFSET:
				 	s->add.l = curtag->ti_Data; /* floats are also copied */
				 	break;
				 case I2SEN_SIGN_BITOFF:
				 	s->signbit = curtag->ti_Data;
				 	break;
				 case I2SEN_EXTRMASK:
				 	s->extractmask = curtag->ti_Data;
					break;
				 case I2SEN_DIVBY:
				 	s->usedivby = 1;
				 	break;
				 case I2SEN_WAKEUP:
				 	s->wakeup = (UBYTE)curtag->ti_Data;
				 	break;

				 /* these are the custom functions for sensors that need
				    more complex math on top of their read values
				    new entry: - add I2SENC_... to i2cclass_sensor.h,
				               - add entry to confcustomtag in config.c
					       - provide the function (see custom_BMP280T() as
					         an example)
				 */
				 case I2SEN_CUSTOM:
				 	if( curtag->ti_Data == I2SENC_BMP280T )
				 		s->cfunc = custom_BMP280T;
				 	if( curtag->ti_Data == I2SENC_BMP280P )
				 		s->cfunc = custom_BMP280P;
				 	if( curtag->ti_Data == I2SENC_BME680T )
				 		s->cfunc = custom_BME680T;
				 	if( curtag->ti_Data == I2SENC_BME680P )
				 		s->cfunc = custom_BME680P;
				 	if( curtag->ti_Data == I2SENC_BME680H )
				 		s->cfunc = custom_BME680H;
				 	if( curtag->ti_Data == I2SENC_TK60T )
				 		s->cfunc = custom_TK60TEMP;
				 	break;
				 default:
				 	D(("unhandled Tag %lx\n",curtag->ti_Tag));
				 	break;
				}
			}
		}
	}

	/* postproc: compute IDs */
	{
		const LONG types[] = {I2C_VOLTAGE,I2C_CURRENT,I2C_TEMP,I2C_FAN,I2C_PRESSURE,I2C_POWER,I2C_HUMIDITY,0};
		LONG i,count;
		ULONG id;
		UBYTE *p;

		for( i = 0 ; types[i] != 0 ; i++ )
		{
			count = 0;
			/* search through list */
			s=(struct i2s_sensor*)i2c_sensorbase->sensors.lh_Head;
			while( s->n.mln_Succ )
			{
				if( s->type == types[i] )
				{	
					id = 0;
					if( (p=s->name) )
					{
						while( *p )
							id = ((id^0x7f3e)>>1) + (((ULONG)*p++)<<8);
					}
					id &= 0xffff;
					id |= ((ULONG)(s->i2caddr))<<24;
					id |= count<<20;
					id |= ((s->type - I2C_VOLTAGE)&0xf)<<16;
					s->id = id;
					count++;
				}
				s=(struct i2s_sensor*)s->n.mln_Succ;
			}
		}
	}

#if 0
	/* compute something out of the I2C Address, Index and the sensor name */
	res = 0;
	if( (p=s->name) )
	{
		while( *p )
			res = ((res^0x7f3e)>>1) + (((ULONG)*p++)<<8);
	}
	res |= ((ULONG)(s->i2caddr))<<24;
	res |= index<<20;
#endif
	return 1;
}




ASM static LONG i2c_SensorConfigure( ASMR(a6) BASETYPE *i2c_sensorbase  ASMREG(a6) )
{
  BPTR lock;
  LONG ret;
#if 0
  LONG more;
  struct ExAllControl *eac;
  struct ExAllData ead,*cur;
#endif
  const STRPTR a = "~(#?.info)";
  const STRPTR dp= "Devs:Sensors/";
  BYTE path[108+32];
  BYTE pat[24];
  
  /* D(("Sensor Configure\n")); */
  
  ParsePatternNoCase( a, pat, 24 );

  lock = Lock( dp, ACCESS_READ );

  ret  = 0;
  if( lock )
  {
    /* D(("Have Lock\n")); */
#if 1
	{
	 struct FileInfoBlock fib;
	 
	 if( Examine( lock, &fib ) )
	 {
		while( ExNext( lock, &fib ) )
		{
			if( fib.fib_DirEntryType < 0 )
			{
				if( MatchPatternNoCase( pat, fib.fib_FileName ) )
				{
					/* main part: read each config */
					StringCopy( path, dp );
					StringCat( path, fib.fib_FileName );
					D(("%s\n",path));
					
					if( !(i2c_sensorbase->deviceconfig = config_parse(BASEARG 
					                                     (BYTE*)path,
					                                     i2c_sensorbase->deviceconfig) )
					  )
					{
						D(("Config parse error\n"));
					}
				}
				else
				{
					D(("Reject Pattern Match %s\n",fib.fib_FileName));
				}
			}
		}
	 }
	}
#else
	eac = (struct ExAllControl*)AllocDosObject( DOS_EXALLCONTROL, (0) );
	if( eac )
	{
	    D(("Have EAC\n"));
		eac->eac_LastKey = 0;
		eac->eac_MatchString = pat;
		do
		{
			more = ExAll( lock, &ead, sizeof(ead), ED_NAME, eac ); 
			if( eac->eac_Entries > 0 )
			{
			    D(("Have EAC Entries\n"));

				cur = &ead;
				do
				{
					/* main part: read each config */
					StringCopy( path, dp );
					StringCat( path, cur->ed_Name );
					D(("%s (%ld)\n",path,eac->eac_Entries));

					if( !(i2c_sensorbase->deviceconfig = config_parse(BASEARG 
					                                     (BYTE*)path,
					                                     i2c_sensorbase->deviceconfig) )
					  )
					{
						Printf("Config error\n");
						return 0;
					}
				}
				while( (cur=cur->ed_Next) );
			}
			else
			 more = 0;
		}
		while(more);	 
	 
	  	FreeDosObject( DOS_EXALLCONTROL, eac );
	}
#endif
	UnLock( lock );

	/* translate config tags into internal struct 
	   less flexible but faster (and private, too)
	*/
	i2c_SensorTranslateConfig( i2c_sensorbase );

	/* initialize all sensors */
	i2c_SensorInit( i2c_sensorbase );

	ret = 1;
  }
  
  return ret;
}


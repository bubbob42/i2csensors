/*
  simple frontend test for I2C Sensors library
  
  (C) 2018 Henryk Richter

  This is a short demonstration for the I2C Sensor 
  library API. First, the count of sensors of specific
  types is printed. Then all sensors are polled and
  the results will be printed as well.

*/
#include <dos/dos.h>
#include <dos/exall.h>
#include <proto/dos.h>
#include <exec/memory.h>
#include <proto/exec.h>

#include <proto/i2csensors.h>
#include <libraries/i2csensors.h>

#define nTypes 7
const LONG senlist[nTypes] = { I2C_VOLTAGE,I2C_CURRENT,I2C_TEMP,I2C_FAN,I2C_PRESSURE, I2C_POWER, I2C_HUMIDITY };
const char*sennames[nTypes]= { "Voltage",  "Current",  "Temperature","Fan","Pressure","Power",   "Humidity" ,}; 
LONG sennum[nTypes];

struct Library *I2CSensorsBase;

int main( int argc, char**argv )
{

	I2CSensorsBase = OpenLibrary( (STRPTR)SENSORSNAME, 1 );
	if( !I2CSensorsBase )
	{
		Printf("cannot open %s\n",(ULONG)SENSORSNAME );
		return 10;
	}
	Printf("I2C sensors opened.\n");

#if 0
	/* new feature in v2: semi constant sensor IDs
	
	*/
	if( I2CSensorsBase->lib_Version >= 2 )
	{
		LONG idx,tp;
		ULONG id = i2c_SensorID( I2C_VOLTAGE, 1 ); /* get ID of second voltage sensor */

		tp = i2c_SensorTypeFromID( id, &idx ); /* reverse example: calculate type/index from ID */

		Printf("ID %lx Type %lx Index %ld\n",id,tp,idx);

		idx = i2c_ReadSensor( I2C_ID, id, NULL, NULL ); /* read sensor by ID */

		Printf( (STRPTR)"%ld.%04ld\n",idx>>16,((idx&65535)*15259)/100000 );
	}
#endif

	{
	 LONG i,j,val;
	 UWORD sign;
	 BYTE *unit,*name;
	 
	 /* Loop 1: find number of sensors for each type */
	 for( i=0 ; i < nTypes ; i++ )
	 {
		sennum[i] = i2c_SensorNum( senlist[i] );
		Printf("%s: %ld\n",(ULONG)sennames[i],sennum[i]);
	 }

	 /* Loop 2: poll all sensors 
	            Note: selective sensor reading is explicitly supported, of course.
	 */
	 for( i=0 ; i < nTypes ; i++ )
	 {
		for( j=0 ; j < sennum[i] ; j++ )
		{
			val  = i2c_ReadSensor( senlist[i], j, &unit, &name);
			sign = (val<0) ? '-'<<8 : 0;
			val  = (val<0) ? -val : val;
			Printf( (STRPTR)"%s %s %s%ld.%04ld %s\n",(ULONG)name,(ULONG)sennames[i],
			       (ULONG)&sign,
			       val>>16,
			       ((val&65535)*15259)/100000,
			       (ULONG)unit );
		}
	 }
	 

	}

	CloseLibrary( I2CSensorsBase );

	return 0;
}

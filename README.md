# Amiga I2C Utilities

This repository contains several utilities, libraries and sub-projects for I2C slaves
connected to Amiga computers. 

The obvious prerequisite for the content of this project collection is a working I2C hardware/software installation,
i.e. proper LIBS:i2c.library present and tested (e.g. using I2CScan). Several approaches are available
to get I2C working with your Amiga(s). Please consult the hardware documentation in the
http://aminet.net/package/docs/hard/i2clib40 package for convenient options (parallel, serial or floppy
port). The development of this package was done with a cloned ICY board (a1k.org project). Another
variant is the A3000 daughterboard by Matthias Heinrichs which contains the ICY controller as a virtual
5th Zorro slot.

![Fanny controller in A3000 with MatzeDB](https://gitlab.com/HenrykRichter/i2csensors/raw/master/Fanny/PCB/Pics/Minimal_A3000_MatzeDB.JPG)

## Amiga I2C Sensors

This project contains a shared library for convenient access to I2C (TWI) sensors connected to classic 68k Amiga computers. 

This is the proposal for a somewhat generic I2C based sensor framework.
i2csensors.library is to be copied to LIBS:

Furthermore, the library expects sensor definitions in DEVS:Sensors/
For starters, just copy the relevant contents of the Sensors directory from
devs/ to DEVS:Sensors/
It contains definitions for several chips:
* 0xD0 DS3231 RTC (as temperature sensor)
* 0x98 LTC2990 for voltages and temperatures
* 0x9E LTC2990 for voltages and temperatures on TK060
* 0xEC BMP280 for temperature and pressure
* 0xEE BME680 for temperature, pressure and humidity
* 0xB8 AM2320 for temperature and humidity
* 0xA0/0xA2 MAX31760 and 0x9A LTC2990 on the fan controller Fanny

Most of the supported chips are configured for their default I2C address 
(exceptions: second MAX31760 and LTC2990 on Fanny). Use I2CScan from
the I2c package to verify the presence of chips on your bus.

The library scans all files in DEVS:Sensors upon startup. For practical
purposes, it's preferable to break down the sensors definitions into one
file per chip although multiple chips can be defined within each file.

A simple demonstration/test program is available. Please see simplesensors
and simplesensors.c.

## Development notes:

Include files for the common compilers (SAS/C,gcc,vbcc) are provided in
the "include" directory. The test program (simplesensors.c) should be
self-explaining. 

Another executable being built by Makefile (mainly for cross development
but also ADE compatible) or SMakefile is "I2CSensors". This one is aimed
at reduced turnaround time. It links the library functions directly into
the executable for easier debugging. Please don't deploy that one.
Also provided is a tool called "diagnostics" that will check settings 
and library bases for potential issues.



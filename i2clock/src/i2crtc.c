/*
  i2crtc - test program for I2C based RTC reading/writing

  author: Henryk Richter <henryk.richter@gmx.net>

*/
#include <dos/dos.h>
#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/utility.h>
#define __CONSTLIBBASEDECL__
#include <proto/i2c.h>

#include "i2crtc.h"
#include "i2cclass_rtc.h"
#include "compiler.h"

/*----------------- safeguard against undesired compilation/linking ----------*/
/* problem: if linking without startup code, gcc2.95 will put constants from 
            functions before the functions themselves in the object files. To ensure
	    that the program will work as intended, a simple two-step start
	    is applied.
*/
SAVEDS int progstart(void);

#if 1
int startup(void)
{
 return progstart();
}
#else
/* variant b) put a structure that actually does a JMP 
   -> doesn't work with -fbaserel in GCC
*/
struct _safeguard_ {
	 UWORD magic;
	 ULONG ptr;
};
extern const struct _safeguard_ safeguard;
const struct _safeguard_ safeguard  = { 0x4ef9, (ULONG)progstart }; /* jmp progstart */
#endif
/*----------------- safeguard against undesired compilation/linking ----------*/

#define VERSION  0
#define REVISION 7
#define DATE     14.06.2018

/*----------------- options template -----------------------------------------*/
#define OPT_TEMPLATE "CHIP=DEV/K,LOAD/S,SAVE/S,SHOW/S,SCAN/S"

struct progopts {
	BYTE *devname;
	ULONG loadmode; /* same as setclock load */
	ULONG savemode; /* same as setclock save */
	ULONG showmode; /* show time in RTC */
	ULONG scanmode; /* scan I2C devices and match up with supported chips */
};

/*----------------- options template -----------------------------------------*/


/*----------------- local protos and variables -------------------------------*/
extern void MemSet( VOID *, UBYTE, ULONG );
extern int  my_scan_i2c( long);
extern int  my_show_i2c( struct i2c_rtcbase* , ULONG, long );
extern int  my_load_i2c( struct i2c_rtcbase* , ULONG, long );
extern int  my_save_i2c( struct i2c_rtcbase* , ULONG, long );

extern UBYTE BINBCD( USHORT );


const char dosname[];
const char utilityname[];
const char printfmt[];
const char i2cname[];
const char opt_template[];
const char unknownstr[];

SYSBASETYPE        *SysBase;
struct DosLibrary  *DOSBase;
UTILITYBASETYPE    *UtilityBase;

#define xstr(a) str(a)
#define str(a) #a

const char verstring[] = "$VER: I2CClock " xstr(VERSION) "." xstr(REVISION) " (" xstr(DATE) ")";
const char rtcshowstr[] = "RTC %ld-%ld-%ld %2ld:%02ld:%02ld\n";
const char rtcreadfail[] = "RTC read failure\n";
const char rtcwritefail[]= "RTC write failure\n";


/*----------------- main function --------------------------------------------*/
SAVEDS int progstart( void )
{
/* struct Library *IntBase = (struct Library*)OpenLibrary("intuition.library",37); */
/* int i; */
 int mode,ret;
 struct RDArgs *rdargs;
 struct progopts opts;

 SysBase = *(SYSBASETYPE**)0x4L;
 DOSBase = (struct DosLibrary*)OpenLibrary( (char*)dosname,37);
 mode    = 0;

 if( !DOSBase )
 	return 20;
 
 UtilityBase = (UTILITYBASETYPE*)OpenLibrary( (char*)utilityname,37);

 /* parameters */
 MemSet( &opts, 0, sizeof( struct progopts ));
 if( (rdargs = ReadArgs( (char*)opt_template,(LONG*)&opts,NULL) ) )
 {
  mode  =  (opts.showmode) ? 1 : 0;
  mode |=  (opts.loadmode) ? 2 : 0;
  mode |=  (opts.savemode) ? 4 : 0;
  mode |=  (opts.scanmode) ? 8 : 0;
 }
 else
  Printf("Syntax: %s\n",(ULONG)opt_template);

 /* load/show/save */
 if( (mode & 7) && !(opts.devname) )
 {
	Printf("This operational mode needs CHIP= in arguments\nSyntax: %s\n",(ULONG)opt_template);
	ret = 10;
	goto Quit;
 }

 /* do what was asked for */
 if( mode & 8 ) /* scan */
 {
	my_scan_i2c(1);
 }
 else
 {
	if( !mode )
	{
	      Printf("Operation required: LOAD,SAVE,SHOW or SCAN\n");
	}
	else
	{
	  struct i2c_rtcbase* rtc;
	  ULONG  devid;

	  rtc = i2c_RTCOpen();
	  if( !rtc )
	  {
	      Printf("Cannot open I2C RTC functions. LIBS:i2c.library missing?\n");
	  }
	  else
	  {
	      devid = i2c_RTCChipIDbyName( rtc, opts.devname );
	      if( !devid )
	           Printf("Unknown Device %s\n", (ULONG)opts.devname);

	      if( mode & 1 ) /* show */
	      {
	           my_show_i2c(rtc,devid,1);
	      }

	      if( mode & 2 ) /* load */
	      {
	           my_load_i2c(rtc,devid,1);
	      }
	      if( mode & 4 ) /* save */
	      {
	           my_save_i2c(rtc,devid,1);

	      }
	      i2c_RTCClose(rtc);
	  }
	}
 }


 ret = 0;

Quit:

 if( rdargs ) FreeArgs(rdargs);

 CloseLibrary( (struct Library*)DOSBase);

 return ret;
}


int my_show_i2c( struct i2c_rtcbase*rtc, ULONG devid, long verbose )
{
	struct ClockData clk;

	if( i2c_RTCRead( rtc, devid, &clk ) != I2C_OK )
	{
		Printf( (char*)rtcreadfail);
		return 0;
	}

	Printf(     (char*)rtcshowstr,
	            clk.mday,clk.month,clk.year,
		    clk.hour,clk.min,clk.sec );

	return 1;
}


int my_load_i2c( struct i2c_rtcbase*rtc, ULONG devid, long verbose )
{
	struct ClockData clk;

	if( i2c_RTCRead( rtc, devid, &clk ) != I2C_OK )
	{
		Printf( (char*)rtcreadfail);
		return 0;
	}

	i2c_RTCSetSystemTime( rtc, &clk );

	return 1;
}


int my_save_i2c( struct i2c_rtcbase*rtc, ULONG devid, long verbose )
{
	struct ClockData clk;

	i2c_RTCGetSystemTime( rtc, &clk );

	if( i2c_RTCWrite( rtc, devid, &clk ) != I2C_OK )
	{
		Printf( (char*)rtcwritefail);
		return 0;
	}


	return 1;
}


int  my_scan_i2c( long verbose)
{
 LONG total = 0;
 USHORT dummy=0;
 LONG id,rd,wr,found;
 struct TagItem *rtc_list,*tlist,*devtg,*tg,*ven;
 struct Library *I2C_Base;
 static struct TagItem unk = {I2CRTC_VENDOR, (ULONG)unknownstr };

 rtc_list = i2c_RTCChips();
 I2C_Base = OpenLibrary( "i2c.library",39 );

 if( !I2C_Base )
 {
 	Printf("Cannot open i2c.library V39\n");
	return total;
 }

 for( id = 0; id < 256 ; id+=2 )
 {
  rd = ( ReceiveI2C( id+1, 1, (BYTE*)&dummy) & 0xff) ? 1 : 0;
  wr = ( SendI2C(    id,   0, (BYTE*)&dummy) & 0xff) ? 1 : 0;

  if( (rd&wr) )
  {
	/* now check array for ID */
	Printf("Chip Address 0x%lx/0x%lx, candidate types:\n",id,id+1);

	found = 0;
	tlist = rtc_list;
	while( (tg = NextTagItem(&tlist)) )
	{
		/* go through device taglist */
		if( tg->ti_Tag == I2C_DEVENTRY )
		{
			/* in each device, search for DEVICEID tag and verify ID */
			devtg = (struct TagItem*)tg->ti_Data;
			if( (tg = FindTagItem( I2C_RDID , devtg ) ))
			{
				if( tg->ti_Data == (id+1) )
				{
					ven = FindTagItem( I2CRTC_VENDOR, devtg );
					tg  = FindTagItem( I2CRTC_DEVICE, devtg );
					if( !ven )
						ven = &unk;
					if( !tg )
						tg = &unk;

					Printf("Vendor: %s Chip: %s\n",ven->ti_Data,tg->ti_Data );
					found = 1;
					total++;
				}
			}
		}
	}

	if( !found )
		Printf("No RTC candidate.\n");
  }
 }

 if( !total )
 {
	Printf("No supported RTC chips found\n");
 }

 CloseLibrary( I2C_Base );

 return total;
}



void MemSet( VOID *ptr, UBYTE c, ULONG size )
{
 UBYTE *p = (UBYTE*)ptr;

	while( size-- )
		*p++ = c;
}



/* some strings down here */
const char opt_template[] = OPT_TEMPLATE;
const char dosname[] = "dos.library";
const char printfmt[] = "%ld -> %lx\n";
const char i2cname[] = "i2c.library";
const char unknownstr[] = "unknown";
const char utilityname[] = "utility.library";


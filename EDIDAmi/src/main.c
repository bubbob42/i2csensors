/*
 * Copyright 2019 Henryk Richter
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sub license,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
#include <stdio.h>
#include <dos/dos.h>
#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/utility.h>

/* from XFree86/Xorg */
#include "edid.h"
#include "displaymode.h"
#include "xf86DDC.h"
#include "xf86.h"
#include "xf86str.h"
#include "xf86Modes.h"

#include "edidread.h"
/*
  history:
  1.1 - implemented DUMP=SAVE keyword to save EDID to disk
  1.0 - first upload to gitlab.com

  opts:
   - DDC (explicit)
   - FILE (EDID from file)
   - MAXCLOCK in kHz (exclude modes where the graphics card clock can't keep up)
   - MAXSIZE=MAXWH (exclude modes which the graphics card can't supply)

   - XMODELINE=X
   - P96MODELINE=P96 (default)
   
   - SIZE=WH (compute custom mode using CVT or GTF)
   - DOUBLESCAN=DSCAN (for custom mode)
   - INTERLACE=I (for custom mode)
   - RATE=HZ (
   - CVT
   - GTF
*/

/*----------------- needed by xf86Mode.c -----------------*/
const int xf86NumDefaultModes = 0;
const DisplayModeRec xf86DefaultModes[1];
/*----------------- needed by xf86Mode.c -----------------*/

/*----------------- options template -----------------------------------------*/
#define OPT_TEMPLATE "DDC/S,FILE/K,XMODELINE=X/S,P96MODELINE=P96/S,VERBOSE=V/S,MAXCLOCK/K/N,MAXWIDTH=MAXW/K/N,MAXHEIGHT=MAXH/K/N,WIDTH=W/K/N,HEIGHT=H/K/N,RATE=HZ/K/N,DOUBLESCAN=DSCAN/S,INTERLACE=I/S,CVT/S,GTF/S,DUMP=SAVE/K"
struct progopts {
	ULONG useddc;     /* use DDC              */
	BYTE *filename;   /* read EDID from File  */
	ULONG xmodes;     /* print X modelines    */
	ULONG p96modes;   /* print P96 modespecs  */
	ULONG verbose;    /* increase verbosity level */
	ULONG *maxclock;  /* max clock in MHz     */
	ULONG *maxwidth;  /* max. gfx card width  */
	ULONG *maxheight; /* max. gfx card height */
	ULONG *width;     /* mode width           */
	ULONG *height;    /* mode height          */
	ULONG *vrefresh;  /* vertical refresh     */
	ULONG dscan;      /* doublescan           */
	ULONG interlace;  /* interlaced mode      */
	ULONG cvt;        /* use CVT timing rules */
	ULONG gtf;	  /* use gtf timing rules */
	BYTE  *ofilename; /* save EDID to file    */
};
const char opt_template[];
/*----------------- options template -----------------------------------------*/

LONG edidfromfile( BYTE *fname, UBYTE *buffer, ULONG bytes );
LONG edidtofile( BYTE *fname, UBYTE *buffer, ULONG bytes );
void p96PrintModeline( LONG scrnIndex, DisplayModePtr Mode);
void ValidateModesSize(ScrnInfoPtr, DisplayModePtr, int, int, int);

/*----------------- program starts here --------------------------------------*/
int main( int argc, char **argv )
{
 /* const char *fname = "test.edid"; */
 unsigned char edidbuf[128]; /* TODO: >128 bytes support */
 struct RDArgs *rdargs;
 struct progopts opts;
 int    ret = 0;
 xf86MonPtr ddcmon;
 DisplayModePtr ddcdisp = NULL;
 int scrnIndex = 0;

 memset( &opts, 0, sizeof( struct progopts ));
 if( (rdargs = ReadArgs( (char*)opt_template,(LONG*)&opts,NULL) ) )
 {
  if( !(opts.xmodes) && !(opts.p96modes) )
   opts.p96modes = 1;
 }
 else
 {
  Printf("Syntax: %s\n",(ULONG)opt_template);
  return 1;
 }

 if( !(opts.width) || !(opts.height) )
 {
 /*----------- start read/load/parse EDID -----------------------------------------*/
  do
  {
   /* input EDID from I2C or file */
   if( (opts.useddc) || (!opts.filename) )
   {
     if( !(opts.useddc) && (opts.width) )
      break;
  	ret = (opts.verbose) ? 1 : 0;
	if( i2c_readedid( edidbuf, 128, ret ) < 128 )
		break;
	if( opts.ofilename )
		edidtofile( opts.ofilename,edidbuf, 128 );
   }
   else
   {
    if( 128 != edidfromfile( opts.filename, edidbuf, 128 ) )
   	break;
   }

   {

    ddcmon = xf86InterpretEEDID( scrnIndex, edidbuf);
    if( !ddcmon )
    {
  	Printf("cannot create Monitor specs from EDID\n");
  	break;
    }
    else
    {
	ddcdisp = xf86DDCGetModes( scrnIndex, ddcmon );
	printf("Monitor size %ld cm x %ld cm\n",(long)ddcmon->features.hsize,(long)ddcmon->features.vsize);
	if( !ddcdisp )
		Printf("No valid Modes found\n");

	/* prune modes ? */
	if( (opts.maxwidth) || (opts.maxheight) )
	{
		int mw = (opts.maxwidth)  ? *opts.maxwidth  : 0;
		int mh = (opts.maxheight) ? *opts.maxheight : 0;
		
		printf("mw %d mh %d\n",mw,mh);
		
		ValidateModesSize( NULL, ddcdisp, mw, mh, 0);
		xf86PruneInvalidModes( NULL, &ddcdisp, (opts.verbose) ? 1 : 0 );
	}
	if( (opts.maxclock) )
	{
		int minc = 1;
		int maxc = *opts.maxclock; /* in kHz */
		
		xf86ValidateModesClocks( NULL, ddcdisp, &minc, &maxc, 1);
		xf86PruneInvalidModes( NULL, &ddcdisp, (opts.verbose) ? 1 : 0 );
	}
    }
   }
  }
  while(0);
  /*----------- end read/load/parse EDID ------------------------------------------*/
 } /* if( !(opts.width) || !(opts.height) ) */

 /*----------- start calculate custom mode ---------------------------------------*/
 if( (opts.width) && (opts.height) )
 {
  int w,h,r;
  DisplayModePtr dispcvt = NULL, dispgtf = NULL;
  
  w = *opts.width;
  h = *opts.height;
  r = (opts.vrefresh) ? *opts.vrefresh : 60;

  if( !(opts.cvt) && !(opts.gtf) ) /* default to cvt */
  	opts.cvt = 1;

  if( opts.cvt )
  {
	dispcvt = xf86CVTMode( w, h, (float)r, (opts.interlace)?1:0, 0, ( opts.dscan )?1:0 );
	ddcdisp = xf86ModesAdd( ddcdisp, dispcvt );
  }
  if( opts.gtf )
  {
	dispgtf = xf86GTFMode( w, h, (float)r, (opts.interlace)?1:0, 0, ( opts.dscan )?1:0);
	ddcdisp = xf86ModesAdd( ddcdisp, dispgtf );
  }


 }
 /*----------- end calculate custom mode   ---------------------------------------*/
 

 /* print modes, X11 and/or P96 format */
 if( opts.xmodes )
 {
	DisplayModePtr Mode = ddcdisp;
        while(Mode)
        {
		if( Mode->VRefresh < 0.1 )
			Mode->VRefresh = xf86ModeVRefresh(Mode);
		xf86PrintModeline(scrnIndex, Mode);
		Mode = Mode->next;
	}
 }
 if( opts.p96modes )
 {
	DisplayModePtr Mode = ddcdisp;
        while (Mode)
        {
		if( Mode->VRefresh < 0.1 )
			Mode->VRefresh = xf86ModeVRefresh(Mode);
	 	p96PrintModeline(scrnIndex, Mode);
	 	Mode = Mode->next;
	}
 }
 /* clean up */
 while( ddcdisp )
 {
	xf86DeleteMode( &ddcdisp, ddcdisp );
 }
 if( ddcmon )
	 free(ddcmon);


 if( rdargs ) FreeArgs(rdargs);

 return ret;
}

void p96PrintModeline( LONG scrnIndex, DisplayModePtr Mode)
{

  printf("\n");
  printf("Picasso96Mode values for %ld x %ld\n",(LONG)Mode->HDisplay,(LONG)Mode->VDisplay);
  printf("  Clock: %3.2f    Interlace: %3s\n", Mode->Clock / 1000., (Mode->Flags & V_INTERLACE) ? "YES" : "NO " );
  printf("                 DoubleScan: %3s\n", (Mode->Flags & V_DBLSCAN) ? "YES" : "NO " );
  printf("\n");
  printf("  Timings      | horiz. | vert.\n");
  printf("  ------------------------------\n");
  printf("  FrameSize    | %6d | %6d\n", Mode->HTotal, Mode->VTotal);
  printf("  BorderSize   |      0 |      0\n");
  printf("  Position     | %6d | %6d\n", Mode->HSyncStart - Mode->HDisplay, Mode->VSyncStart - Mode->VDisplay);
  printf("  SyncSize     | %6d | %6d\n", Mode->HSyncEnd   - Mode->HSyncStart, Mode->VSyncEnd - Mode->VSyncStart);

  if( Mode->Flags & (V_PCSYNC|V_NCSYNC) )
   printf("  SyncPolarity Composite %6s\n",(Mode->Flags & V_NCSYNC) ? "YES" : "NO " );
  else
   printf("  SyncPolarity | %6s | %6s\n", (Mode->Flags & V_NHSYNC) ? "YES" : "NO ", (Mode->Flags & V_NVSYNC) ? "YES" : "NO ");

  printf("     Frequency | %3.0fkHz | %4.0fHz\n", (float)xf86ModeHSync(Mode), Mode->VRefresh );
}

LONG edidfromfile( BYTE *fname, UBYTE *buffer, ULONG bytes )
{
	FILE *bla;
	size_t len;
	
 	bla = fopen(fname, "rb" );
 	if( !bla )
 	{
		printf("cannot open input file %s\n",fname);
		return 0;
	}

	len = fread( buffer, 1, bytes, bla );

	fclose( bla );

	if( len != bytes )
	{
		Printf("only %ld Bytes read (requested 128)\n",(ULONG)len);
		return 0;
	}

	return (LONG)len;
}

LONG edidtofile( BYTE *fname, UBYTE *buffer, ULONG bytes )
{
	LONG ret = 0;
	FILE *bla;
	size_t len;

	bla = fopen(fname, "wb" );
	if( !bla )
	{
		printf("cannot open output file %s\n",fname);
		return ret;
	}
	len = fwrite( buffer, 1, bytes, bla );

	fclose( bla );

	if( len != bytes )
		printf("cannot write EDID to file %s\n",fname);
	else
		ret = (LONG)len;

	return ret;
}

void ValidateModesSize(ScrnInfoPtr pScrn, DisplayModePtr modeList,
                       int maxX, int maxY, int maxPitch)
{
    DisplayModePtr mode;

    if (maxPitch <= 0)
        maxPitch = MAXINT;
    if (maxX <= 0)
        maxX = MAXINT;
    if (maxY <= 0)
        maxY = MAXINT;

    for (mode = modeList; mode != NULL; mode = mode->next)
    {
        if( (xf86ModeWidth(mode,  RR_Rotate_0) > maxPitch) ||
            (xf86ModeWidth(mode,  RR_Rotate_0) > maxX)     ||
            (xf86ModeHeight(mode, RR_Rotate_0) > maxY)       )
                mode->status = MODE_BAD_WIDTH;

        if (mode->next == modeList)
            break;
    }
}


/* some strings down here */
const char opt_template[] = OPT_TEMPLATE;
const char verstring[] = "$VER: EDIDAmi 1.1 (14.05.2019) VGA Monitor EDID reader and parser for AmigaOS3.x (C) Henryk Richter";

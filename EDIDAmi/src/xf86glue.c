#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include "adapter_types.h"

float rint(float x)
{
	volatile int bla = (int)x;
	return (float)( bla );
}

/* #define XNFasprintf sprintf */
int XNFasprintf(char **ret, const char *format, ...)
{
 char *tmp = (char*)malloc( 128 ); 
 int len;
 va_list argp;

 *ret = tmp;
 if( !tmp )
 	return 0;

 va_start(argp, format);
 len = vsnprintf(tmp,127,format,argp);
 /* vprintf(format,argp); */
 va_end(argp);

 return len;
}

void xf86DrvMsg(int scrnIndex, MessageType type, const char *format, ...)
{
#if 1
 va_list argp;
 va_start(argp, format);
 vprintf(format,argp);
 va_end(argp);
#else
/* poor man's printf, incomplete */
 va_list argp;
 va_start(argp, format);
  while (*format != '\0') {
    if (*format == '%') {
      format++;
      switch( *format )
      {
	case '%':
       	 putchar('%');
	 break;
	case 'c':
	 {
	  char char_to_print = va_arg(argp, int);
	  putchar(char_to_print);
	  break;
	 }
	case 'd':
	 {
	  int divider = 1000000000,rem,start;
	  int int_to_print = va_arg(argp, int);
	  if( int_to_print < 0 )
	  {
	  	putchar('-');
		int_to_print = -int_to_print;
	  }
	  if( int_to_print == 0 )
	  	putchar('0');
	  else
	  {
	  	start = 0;
		while( divider )
		{
			rem = int_to_print/divider;
			if( rem | start )
			{
			 putchar( rem + '0' );
			 start = 1;
			}
			int_to_print %= divider;
			divider /= 10;
		}
	  }
	  break;
	 }
	case 's':
	 {
	  char *s = va_arg(argp, char * );
	  fputs(s,stdout);
	  break;
	 }
	default:
	 fputs("N/A", stdout );
	 break;
      }
    } else {
      putchar(*format);
    }
    format++;
  }
  va_end(argp);
#endif
}


void xf86Msg(MessageType type, const char *format,...)
{
 va_list argp;
 va_start(argp, format);
 vprintf(format,argp);
 va_end(argp);
}

void xf86VDrvMsgVerb(int scrnIndex, MessageType type, int verb,
                const char *format, va_list args)
		_X_ATTRIBUTE_PRINTF(4, 0)
{
 vprintf(format,args);
}

void xf86DrvMsgVerb(int scrnIndex, MessageType type, int verb,
		               const char *format, ... )
{
 va_list argp;
 va_start(argp, format);
 vprintf(format,argp);
 va_end(argp);
}

#if 0
#endif



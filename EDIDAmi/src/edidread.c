/*
  read VGA DDC/EDID via I2C Sensors library
  
  (C) 2018 Henryk Richter

  This program reads the EDID configuration
  block from VGA monitors. I2c.library,
  i2csensors.library and an appropriate hardware
  interface are required.

*/
#include <dos/dos.h>
/* #include <dos/exall.h> */
#include <proto/dos.h>
#include <exec/memory.h>
#include <proto/exec.h>

#ifndef __CONSTLIBBASEDECL__
#define __CONSTLIBBASEDECL__
#endif

#include <proto/i2c.h>
#include <proto/i2csensors.h>
#include <libraries/i2csensors.h>


int i2c_readedid_legacy( unsigned char *ddcarray, long size, unsigned long flags );


/*
  read from I2C device
  inputs: array to save the EDID to (>=128 Bytes)
          size of array (>=128)
          flags (may be 0)
*/
int i2c_readedid( unsigned char *ddcarray, long size, unsigned long flags )
{
/*	UBYTE ddcarray[128]; */
	ULONG verbose=0;
	ULONG res = 0;
	struct Library *I2CSensorsBase;

	/* return i2c_readedid_legacy( ddcarray, size, flags ); */ /* testing only */
	
	if( flags & 1 )
		verbose = 1;

	I2CSensorsBase = OpenLibrary( (STRPTR)SENSORSNAME, 1 );
	if( !I2CSensorsBase )
	{
		printf("cannot open %s\n",(ULONG)SENSORSNAME );
		return i2c_readedid_legacy( ddcarray, size, flags );
		/* return 0; */
	}

	{
	 ULONG  id = 0xa0;
	 UBYTE  dummy[4] = {0,0,0,0};

	 i2c_Obtain( (LONG)id );

	 res  = (i2c_Receive( id, 1, dummy) & 0xff) ? 1 : 0;
	 res += (i2c_Send(    id, 1, dummy) & 0xff) ? 1 : 0;
	 if( res == 2 )
	 {
	 		SHORT i;

	 		if( verbose )
				printf("Monitor found at 0x%lx\n",id);
#if 1
			/* DDC2B page */
	 		dummy[0] = 0;
	 		i2c_Send( 0x60, 1, dummy );
#endif
			dummy[0] = 0;
			i2c_Send(id,1,dummy);

			/* this method will force a repeated start condition */
			for( i = 0 ; i < 128 ; i++ )
			{
				if( !( i2c_Receive( id, 1, &ddcarray[i] ) & 0xff ))
					res = 0;
			}
			if( res )
			{
				if( verbose )
				{
					SHORT i;

					Printf("DDC data read\n");
					for( i=0 ; i<128 ; i++ )
					{
						if( (i) && !(i&0x7) )
							printf("\n");
						printf( "%02lx ",(LONG)ddcarray[i] );
					}
					printf("\n");
				}
				res = 128;
			}
			else
				printf("error reading data\n");
	 }
	 else
		printf("Error: Monitor not found at 0x%lx\n",id);

	 i2c_Release( (LONG)id );
	}

	CloseLibrary( I2CSensorsBase );

	if( res < 8 )
		res = 0;

	return res;
}

/* same as above, just by using i2c.library instead of i2csensors.library */
int i2c_readedid_legacy( unsigned char *ddcarray, long size, unsigned long flags )
{
	const STRPTR nm = "i2c.library";
	struct Library *I2C_Base;
	ULONG verbose=0;
	ULONG res = 0;
	
	if( flags & 1 )
		verbose = 1;
	
	I2C_Base = OpenLibrary( (STRPTR)nm, 39 );
	if( !I2C_Base )
	{
		printf("cannot open %s\n",(ULONG)nm );
		return 0;
	}

	{
	 ULONG  id = 0xa0;
	 UBYTE  dummy[4] = {0,0,0,0};

	 res  = (ReceiveI2C( id, 1, dummy) & 0xff) ? 1 : 0;
	 res += (SendI2C(    id, 1, dummy) & 0xff) ? 1 : 0;
	 if( res == 2 )
	 {
	 		SHORT i;

	 		if( verbose )
				printf("Monitor found at 0x%lx\n",id);
#if 1
			/* DDC2B page */
	 		dummy[0] = 0;
	 		SendI2C( 0x60, 1, dummy );
#endif
			dummy[0] = 0;
			SendI2C(id,1,dummy);

			/* this method will force a repeated start condition */
			for( i = 0 ; i < 128 ; i++ )
			{
				if( !( ReceiveI2C( id, 1, &ddcarray[i] ) & 0xff ))
					res = 0;
			}
			if( res )
			{
				if( verbose )
				{
					SHORT i;

					Printf("DDC data read\n");
					for( i=0 ; i<128 ; i++ )
					{
						if( (i) && !(i&0x7) )
							printf("\n");
						printf( "%02lx ",(LONG)ddcarray[i] );
					}
					printf("\n");
				}
				res = 128;
			}
			else
				printf("error reading data\n");
	 }
	 else
		printf("Error: Monitor not found at 0x%lx\n",id);
	}

	CloseLibrary(I2C_Base);

	if( res < 8 )
		res = 0;

	return res;
}
